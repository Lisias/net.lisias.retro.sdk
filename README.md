"I choose to code it in BASH.... I choose to code it in BASH and other things in one week, not because it is easy, but because it is HARD!!!"

	#lisíadas #DevOpLife
	(modesto, eu, não?)

------

# Abreviações e Siglas

Usados em todos os documentos do projeto:

* WiP : Work In Progress
* RiP : Research In Progress
* *Archive*, *Cabinet* & *File*
	* Fica estranho em pt_br, mas em inglês o que chamamos de **arquivo** em inglês é *file*, sendo que *cabinet* é um arquivo (no sentido literal em pt_br) de *files*. Um *archive* é uma coleção de documentos (*files*) históricos. Um site FTP é um *archive* por exemplo. O [Internet Archive](https://archive.org/) é outro belíssimo exemplo.
		* um *file* : ![file](./docs/file.png)
			* o termo "arquivo" em pt_br é usado neste projeto com esta semântica.
			* em pt_po, se usa o termo "ficheiro" (que não deixa de ser mais consistente)
		* um *cabinet* : ![file](./docs/cabinet.png)
			* uma coleção organizada de *files*.
			* Um diretório no seu *HD* por exemplo
			* Ou um arquivo `zip`
		* um *archive* : ![file](./docs/archive.jpg)
			* Uma coleção histórica **curada** (de curadoria) de *cabinets* 
			* Neste projeto, também uma entidade com funcionalidades para servir as tais coleções.
	* Essa nomenclatura é consistente em todo o projeto, e será usada mesmo em pt_BR para evitar confusão.

# Configurações e Dependências

São várias, e todas críticas. Leia os documentos de [instalação](https://bitbucket.org/Lisias/net.lisias.retro.pub/src/master/INSTALL.md?fileviewer=file-view-default),  [configuração](https://bitbucket.org/Lisias/net.lisias.retro.pub/src/master/CONFIGURE.md?fileviewer=file-view-default) e [atualização](https://bitbucket.org/Lisias/net.lisias.retro.pub/src/master/UPDATE.nd?fileviewer=file-view-default) no [repositório público](https://bitbucket.org/Lisias/net.lisias.retro.pub) para montar e manter um ambiente de execução para criar Micro Serviços.

Muitas destas dependências se refletem nos clientes locais em Python, já que muitas classes são comuns à ambos os projetos. Leia [isso](docs/clients/console.md) para um passo à passo detalhado caso seu foco seja o *client-side*.

Alguns [exemplos](docs/clients/console.md#webradio2e3) também possuem dependências, que são tratadas à parte pois não são necessárias no *back-end*.

Uma versão *client side*, bem mais enxuta, será publicada tão logo eu estabilize um processo de *deploy* automatizado. ;-)


# Artefatos


## HTTPD2.BASH

Não tem nada que eu possa falar aqui que não possa ser encontrado melhor e mais rápido no repo GIT público. Vai neste [link](http://service.retro.lisias.net/home/) e *serve yourself*.


## net.lisias.retro.file.search.WS

Uma Search Engine para repositórios de arquivos, com foco em repositórios de *Classic Home Computers* (mas os usos não são limitados à eles!). Feito sob medida para rodar em computadores limitados, como *Thin Clients* e o [Raspberry Pi](https://www.raspberrypi.org/) (que por sinal está servindo alguns *archives* neste momento).

Construído num feriado de ócio, que foi enforcado para o próximo fim de semana, que acabou tomando mais uns 2 dias da semana seguinte. E que aos poucos me levou 2 semanas inteiras... :-)

Documentação completa [aqui](./docs/servers/search.md).

## Cliente HTML5 local

O objetivo do Web Service é prover a informação solicitada da forma mais prática, eficiente e eficaz possível. A camada de apresentação é, então, totalmente fora do escopo.

De onde veio o *request*, e para onde vai o *response* não é problema dele.

Então eu precisei montar um cliente minimalista para poder apresentar o dito cujo. Acabou que tive que me virar com o [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing).

Estas páginas foram o resultado desta necessidade.

Documentação completa [aqui](./docs/clients/html.md).


## net.lisias.retro.db.*.WS & HTML

Projetos, ainda em alpha, para prover acesso Read/Only para consultas SQL em bancos de dados públicos, com controle de custos e créditos para consulta - servindo Postgresql e MariaDB. O primeiro banco de dados sendo servido (por ambos) é o ZXDB, do Einar Saukas.

Documentação completa [aqui](./docs/db.md).


## net.lisias.retro.file.proxy.WS

Como nada está tão ruim que não possa piorar, numa tempestade tropical que é típico nesta época aqui em Sampa, fiquei sem sinal de Internet o dia inteiro. Como não dava pra fazer nada sério, tirei uma parte do dia para correr atrás de um dos *feedbacks* dados para o **search.WS**.

Um colega retrocomputeiro, o [Popolon Campus](https://www.facebook.com/popolon.ydoisk?fref=ufi) sugeriu [uma rede cooperativa](https://www.facebook.com/groups/gdmsx/permalink/1869801999919840/) de RPis para servir as buscas.

A sugestão foi mais que excelente, uma vez que sendo o *target* preferencial da *Search Engine* os computadores limitados e/ou baratos que pipocam nos Encontros de Retrocomputação (que nem sempre possuem Internet disponível!!), é fato que não dá para apenas um *host* servir todos os *archives* - a primeira encarnação do **search.WS** exauriu um RPi 2011.12 em apenas 5 dias de vida!!!

A sugestão evoluiu rápido para o que eu chamei de "Confederação de Providers". Um **proxy.WS** (este) centralizaria os requests dos clientes, e através de tabelas internas, faz *round-robin* dos *providers* que suportam a *query* solicitada.

Para dar suporte à esta solução, os *providers* precisam fornecer ***metadados*** sobre os serviços que provêem. Esta funcionalidade foi incluída no projeto do **search.WS** antes da conclusão, já pensando no **proxy.WS** .


Documentação completa [aqui](./README.proxy.md).


## Unidades Confederadas

Um mecanismo (agora) sofisticado foi implementado para permitir que um *Provider* possa registrar-se num *Proxy* para fornecer seus serviços. Os Serviços podem ser pulverizados em vários providers (i.e., um Provider pode servir apenas algumas entidades ou verbos de um *Archive*, e outro outros - e o Proxy se vira pra fazer tudo funcionar).

Um *Proxy*, então, mantêm uma lista de *Providers* e suas capacidades, e ao receber os requests do cliente, redireciona (ou faz ele mesmo o request e ecoa de volta o resultado) a solicitação para um *Provider* cadastrado que seja capaz de servi-la.

Uma vez que cada *Provider* pode fazer parte de mais de uma "Unidade Conferederada" (cujo pivot é o *Proxy*), temos um arremedo de federação - permitindo que *Providers* entrem e saiam do *pool* sem que os clientes precisem se adaptar.

Serviços ***singleton*** foram criados e implementados. Um serviço ***singleton*** é um serviço cuja instância deve ser única na Unidade Confederada (ou mesmo na Confederação Inteira). Um exemplo de serviço ***singleton*** é o WebRadio descrito à seguir.

Documentação completa [aqui](./docs/protocol.md).


## net.lisias.retro.radio.WS

E, como Murphy era um Profeta, piorou mais um pouco. Em (mais um) feriado que me ferrou a produtividade de semana, resolvi fazer uma graçinha no shoutcast que implementei no RPi, e criei um W/S para controle remoto de uma Web Radio. :-)

Projeto ainda alpha.

Documentação completa [aqui](./docs/radio.md).


## net.lisias.retro.JAVA

Seamless integration with Java clients (using Java Serialization) was planned since the beginning, but postponed to prevent extra complexity while core functionalities were still being developed.

Since some of such new core functionalities became viable only using Java libraries (since mature or fully adequate Python native libraries were not found and it's unpractical to reimplement already proven libraries in Python), a new module for Java is
needed.

And yes, I wrote it in english without being aware of that. =D Em outro momento eu traduzo de volta para Português. =D

Documentação completa [aqui](./docs/java.md)


# Andamento do Projeto

* [Known Issues](./KNOWN_ISSUES.md)
* [TODO](./TODO.md) List


# Timeline do Projeto :

Ver [aqui](./HISTORY.md)
