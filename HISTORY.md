TIMELINE DO PROJETO:
--------------------

Ou.... Como um projeto que era pra durar um feriado e um FdS me tomou uma semana inteira, depois o resto do mês, e então o resto do ano, e ainda tá dando o que fazer até hoje! ;-)

* 2018-02-06: RELEASE 1.1 HELLRAISER (re-reissue)
	* Dumb mistake that revealed yet another minor, but important, feature uncompleted.
	* I *really* need to pay more attention to my own test plans. =/
* 2018-02-04: RELEASE 1.1 HELLRAISER (reissue)
	* Some small bugs I left unfixed
	* I need to pay more attention to my own test plans. =/
* 2018-01-31: RELEASE 1.1 HELLRAISER
	* O projeto foi formalmente promovido para **Beta**. #HURRAY!
	* Atualização dos exemplos do SDK para os novos formatos de Metadados
	* Travando a versão do `gevent` para 1.1.2 no `setup.py`, uma vez que os caras soltaram um beta que dá pau na instalação em Python3. =/
* 2018-01-25 a 30:
	* Já que me estrepei com o deploy do que seria a Release 1.1 e furei os prazos, dei continuidade ao refatoramento - adiantando tarefas que iriam para a 1.1.1 :
		* *Annunciators* - os carinhas que controlam quem está registrado com quem ou em quem.
		* *Shutdown* - garantindo que o contexto seja persistido em caso de queda (controlada) do sistema (como em *reboots*).
		* Dando mais um passo rumo à um pool de *Proxies* Confederados (na lata, estou implementando um *Service Bus* de fato e de direito!)
	* Ajustes pesados na Serialização:
		* Em especial, para Enums e outros dados "hardcoded".
		* Padronização nos Metadatados.
* 2018-01-24: RELEASE 1.0.8b
 	* Falha catastrófica no deploy do que seria a RELEASE 1.1. ROLLBACK!
 	* Redeploy da 1.0.8 com a nova infra de RPi3.
 	* Pequenos ajustes para satisfazer as mudanças.
* 2018-01-02 a 20: 
	* Tremendo Overhaul de diversos subsistemas:
		* Infraestutura das Engines.
		* Infraestrutura dos Web Services.
	* O "Protocolo" foi promovido para Beta.
		* Os Metadados foram finalmente normalizados e padronizados.
		* Quebras de interface não são mais esperadas (mas podem acontecer ainda).
* 2017-12-01 a 20: 
	* Search Engines (Front & Back End):
		* O conteúdo de uma imagem de disco está sendo indexado! #HURRAY!
			* Um arquivo "Cabinet" (como são chamados os arquivos que contêm arquivos) é tratado como se fosse um diretório e seu conteúdo é indexado como um arquivo regular
			* Mas o link para download deste último é o link para o arquivo que o contêm. Você procura pelo que você quer, mas na hora do download baixa o treco inteiro.
		* Algumas idiossincrasias internas foram resolvias (aquele papo de extensão de arquivo versus tipo de arquivo)
			* HTML Clients foram atualizados.
			* SDK não precisou de manutenção.
		* No momento, disponível apenas para Imagens de Disco de Apple II, e então só o Asimov está usufruindo.
			* O consumo de memória tá indo pro espaço, no entanto. =/
		* E... Sim, isso levou tempo pra burro pra ser implementado direito! :-D
	* Mais uma Search Engine: [Vireola MSX](http://retro.lisias.net/guests/alexomello/vitrolamsx/)!
	* O Repositório do WoS continua inacessível, e o único mirror oficial conhecido foi desativado.
		* O *Archive* WoS, no momento, está fornecendo links inválidos.
		* Não há prazo para resolver esta questão.
			* Possivelmente o Archive.org resolverá o problema.
				* Mas só vou ter tempo pra ver isso direito na próxima release.
	* Mineradores de Dados:
		* Melhor tratamento de repositórios de sincronia por 'força bruta'.
		* Alguns repositórios forem descobertos como "vivos", e agora são sincronizados regularmente.
		* Ajustes e melhorias no nvg.cpc .
	* Front-Ends:
		* Implementando suporte às novas funcionalidades da Search Engine.
		* Ativando todos os Archives que estavam prontos mas dormentes por falta de infra. #HURRAY!
	* Tremendo Overhaul de diversos subsistemas:
		* Suporte FTP.
		* Producers, Consumers & Synced Consumers para os datasets.
			* Synced Consumers:
				* Batem no ftp.service.retro.lisias.net para ver se os `datasets` possuem atualização e baixa as novidades automaticamente.
		* Serialização `.txt`
			* Agora tá **muito** mais fácil de ler!
		* Tasker
			* Concorrência preemptiva.
			* Melhor controle de tarefas.
			* Contexto individualizado (e customizável) para cada tarefa, incluindo acesso controlado por mutex ao STDIN/STDOUT/STDERR extensível à outros recursos (se desejado).
		* DEBUG_MODE finalmente foi normalizado.
* 2017-10-21: RELEASE 1.0.8
	* Front-End:
		* Melhoria na identificação do cliente html.
		* Atualizando os clientes para a refatoração dos identificadores dos *Archives*.
		* Novos *Archives* ! :-)
		* Buscando do http://lisias.net o logo corrente (o tal do "L") para aproveitar o mecanismo "seasons" do meu site.
	* Back-end:
		* Incluindo o "len" de um array nos *Responses* `.txt` (pra facilidar a vida do pobre desenvolvedor que precisar debugar a bagaça) e outras pequenas melhorias na Serialização.
		* Conexões FTP "stubborn" - agora o processo de *data mining* não morre mais se cair a conexão.
	* Both:
		* Ressucitando o clientes
			* metalab PDP
			* hobbes
		* Novos *Archives*
			* Pigwa Net
			* Apple II Documentation Project
* 2017-09-14: RELEASE 1.0.7
	* Pequena melhoria no front-end 
* 2017-09-08:
	* Erro estúpido no registro do serviço na Confederação
	* ftp.arnold.c64.org voltou!
		* Ressuscitando o micro serviço dele.
		* [link](http://www.lemon64.com/forum/viewtopic.php?t=64576) para o histórico da ocorrência
	* Reativando a Search Engine do Metalab's PDP.
* 2017-08-21:
	* Melhorias no tratamento de Exceções nos Mineradores
	* Finalmente separando as fases de Mineração entre Produto e Consumidor
		* Os consumidores (no caso, `file.search.WS`) agora têm a opção de apenas carregar o *dataset*, e ignorar o *archive* caso o *dataset* não exista ou esteja corrompido.
		* Um produtor pode ser executado esporadicamente na máquina (ou mesmo em outra, exportando via NFS), poupando o *appliance* servidor de ter que torrar processamento com a Mineração.
		* Uma máquina mais rápida, auxiliar e não participante direta da Confederação, pode se encarregada deste serviço! Não precisamos mais esmerilhar os pobres *Raspberries Pi* com isso! :-)
	* Algumas [*Known Issues*](./KNOWN_ISSUES.md) foram resolvidas. :-)
	* Triste mudança: o ftp.arnold.c64.org morreu. :'(
		* Um *mirror* desatualizado (2016/Feb) está sendo levantado em ftp://ftp.service.retro.lisias.net enquanto alternativas não são encontradas.
		* A cópia existente no [Archive Org](https://archive.org/details/arnold.c64.org) é ainda mais antiga (2014/May).
* 2017-05-22: RELEASE 1.0.6c
	* Alguns erros bem estúpidos foram corrigidos (pois é, prometo melhorar)
	* Web Players adicionados para:
		* AmigaScne (mas não ativado, por falta de máquina capaz!)
		* Funet's Amiga
	* Refatoração do FileData para algo mais manutenciável e resiliente.
	* Melhor gerência de exceções durante as atualizações de repositórios.
* 2017-04-21: RELEASE 1.0.6b
	* O *Archive* `metalab.pdp` foi implantado com um erro estúpido. =/ Corrigido.
	* Melhor manipulação dos arquivos de `Magazines` no *Archive* `WOS`, fornecendo no `Description` a quantidade de arquivos/páginas por fascículo/livro e o tamanho total dos arquivos do mesmo.
	* Tornando a bagaça inteira *thread-safe* (finalmente!).
		* Carregamento (e parsing quando necessário) dos *Archives* das Search Engines é feita em paralelo agora. (4)
	* `FileData` com novo formato, com novos dados e suporte para compressão dos `Descriptions` em memória.
	* Tornando as compressões dos *Requests HTTP* configuráveis (os RPi's estavam sofrendo Memory Fault com o ZX e o LZMA)
	* Validação do `FileData` pelo WebRadio
		* Apenas arquivos vindos dos *Archives* conhecidos são aceitos
		* Os mesmos critérios de seleção do que é aceito pelo que não é do *front-end* foram implementados no *back-end*.
		* Sim, eu sou paranóico! :-)
* 2017-03-26: RELEASE 1.0.6
	* Mais *archives* para a *Search Engine*
		* Padua C64
		* Amiga Scne
		* Metalab's PDP directory
		* NVG:
			* BBC
			* CPC
			* Sinclair
			* Sam Coupé
			* VMS
			* Hardware
			* Sounds
	* Mais integrações de *archives* com o Internet Radio:
		* NVG's Sounds
		* Scene.org
		* Hornet
	* Melhorias no *back-end*
		* Melhor e mais versátil *caching* de dados de repositórios
			* "Limpeza" das descrições em repositórios onde é hábito encher o arquivo `.diz` de firula (herança dos tempos de BBS)
		* Customização (e melhor granularidade) no formato dos dados de resposta da Search Engine
			* `options`
			* `fields`
			* Data de atualização do arquivos (sem hora)
			* Ver documentação para detalhes.
* 2017-02-13:
	* Mais um carinho no WebRadio
		* Ajustes no Metadata dele, para facilitar a vida dos clientes (em especial, o cliente console)
		* Ajuste no Proxy, para poder deserializar os metadados ajustados.
	* Cliente Console para o WebRadio!!
		* Descobre sozinho quais Estações estão no ar e pega uma, caso a estação solicitada não exista no momento.
		* Um belo exemplo de uso de asyncio e threading em Python!
* 2017-02-28: RELEASE 1.0.5b
	* Ajustes na U.I. (sdk)/html
		* Página dedicada para o Internet Radio
		* Finalmente fazendo as dialog boxes funcionarem no Mozilla Firefox (que estão enrolando para implementar a tag `dialog` do HTML5).
* 2017-02-10: RELEASE 1.0.5
	* Refatorando o WebRadio
		* Múltiplas fontes para um mesmo canal
		* Organizando melhor os archives/entities
	* Canal AmigaMod
		* Antigo ModLand
		* Necessário para permitir puxar arquivos da AmiNet também
	* Conceito de `fsid` (file server ID) nos objetos `FileData` (aquele que descreve um arquivo em algum repositório no mundo)
		* Permite múltiplas fontes no WebRadio
		* Permitirá um Micro Serviço de Mirror!
	* Clientes locais (SDK)
		* Mais exemplos de clientes (JSON, Força Bruta)
		* Reorganização do exemplo existente
	* Front-Ends HTML (SDK)
		* AmiNet agora também alimenta a WebRadio/AmigaMod!
		* Refatorando pesadamente as bibliotecas JavaScript do /search
			* Resolvendo dependências cruzadas que escaparam
			* Melhor reuso
			* Melhor separação entre as funcionalidades
		* Refatoração do modland.html
			* Adequação para a "nova" WebRadio
			* Busca do conteúdo da Playlist! :)
* 2017-01-31: RELEASE 1.0.4
	* Primeiro cliente linha de comando em Python para a Confederação!
		- Um programinha simples que busca arquivos em **todos** os *Archives* conhecidos e consolida o resultado.
* 2017-01-30: RELEASE 1.0.3
	* Serialização "full duplex"
		- Suporte transparente (ou quase) para deserialização para todos os formatos (e compressões) suportados pela Serialização (menos, óbvio, .TXT)
		- Agora os Micro Services podem falar uns com os outros, se necessário
* 2017-01-24:
	- Listagem parcial de entidades do net.lisias.retro.file.search.WS
		- Importante para os Thin Clients
	- Suporte para compressão das Responses
		- Accepted-Encoding: zx, lzma, bzip2, gzip, z
	- Um DynDNS para usuários da AWS
		- Idealmente era pra isso ficar no Roteador, mas o ambiente Lua daquilo precisa de uma atualização e o OpenWRT com suporte pra isso ainda não saiu.
	- Suporte para Deserialização Automática implementado.
		- Importante para o SDK, e construção de Serviços em cima dos diferentes Micro Serviços.
	- Novos *Archives* do FTP Search Engine
		- Freedos
		- Arnold C64
* 2016-12-15:
	- Melhorando os front-end HTML
		- Ajustes no mecanismo de CSS "dinâmico".
	- Adicionado compressão nos responses (Accept-Encoding : xr, lzma, bzip2, gzip e deflate)
	- `list` com critério no file-search
		- `prefix` : retorna apenas as palavras que comecem com o prefixo fornecido
		- `count` : ordena alfabeticamente e retorna as `n` primeiras palavras.
* 2016-12-14:
	- Melhorando os front-end HTML
		- Gerenciando apresentação para dispositivos com tela Landscape ou Portrait
		- Versões específicas para dispositivos "Thick" e "Thin"
* 2016-12-11: RELEASE 1.0.2
	- Persistência dos dados já "parseados" em arquivos pickle, para agilizar carregamento enquanto o cache for válido
	- Formatos binários "pickle" nos WebServices, para otimizar requests feitos por clientes Python.
	- Annunciators normalizados. Melhor 'handling' do `atexit` com o `gevent`
	- Melhor configuração dos *Archives* nas Search Engines
* 2016-11-28:
	- Front End para os Providers de Queries em Banco de Dados.
* 2016-11-27:
	- Sub rede dedicada para a Confederação.
		- [Foto na Comunidade de Suporte](https://plus.google.com/communities/114533428482860883026)
* 2016-11-20:
	- Novos *Archives* para as Search Engines
		- Garbo (MS-DOS e Turbo Pascal)
		- Hobbes (OS/2)
		- Hornet (PC Demos)
		- TV-Dog (Tandy 1000)
		- x2ftp (Demos e MS-DOS)
* 2016-11-18: RELEASE 1.0 FIRST_BLOOD
	- Dividindo o projeto em
		- PUB - repositorio público para interessados em hospedar serviços
		- SDK - repositório público para interessados em desenvolver serviços
		- DEV - repositório privado para desenvolvimento do kernel.
* 2016-11-13 :
	- Persistência: O Proxy agora salva seu estado durante o shutdown, e tenta restabelecer a Unidade Confederada durante o startup.
	- Postgressql. Um Provider para acesso à banco de dados Postgresql foi implementado.
		- ***net.lisias.retro.db.postgresql.WS*** ou ***psql.WS*** para os íntimos.
* 2016-11-09 :
	- Persistência: Os serviços agora salvam seu estado durante o shutdown, e recuperam durante o start up.
* 2016-11-06 :
	- Implementando ***singleton*** direito no **proxy.WS**.
* 2016-11-05 :
	- Confederação: Entidades ***singleton***
		- Uma tupla [*Archive*, *Entity*] ***singleton*** só pode ter **UM** *Provider* em toda a Unidade Confederada (e possivelmente na Confederação inteira - ainda em estudos).
		- O **proxy.WS** foi reescrito para lidar com entidades **singletone**
		- Também foi adicionado suporte para [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing).
	- **mpd.WS** foi refatorado para emitir metadados que descrevam seus serviços ***singleton*** e URLs de acesso para os Canais (entidades especializadas).
	- A página HTML da ModLand foi instrumentada para permitir manipulação da "Programação" da Web Radio.
		- Previous, Next, Play, Delete - estas ações custam Créditos, que são renovados diariamente
		- As chamadas ao **proxy.WS** são feitas por JSONP onde possível, e por XmlHttpRequest/CORS quando não tem jeito
* 2016-11-02 :
	- **proxy.WS** : pulverização de serviços
	- **mpd.WS** : alpha do Tocador Online de Mídia.
* 2016-10-28 :
	- **proxy.WS** (Confederação de Provedores de Serviços)
	- Protocolo de filiação e desfiliação à Unidades Confederadas.
* 2016-10-26 :
	- Problemas de localStorage nas páginas HTML5
* 2016-10-25 :
	- Incluindo AmiNet (Amiga)
	- Incluindo WhTech (TI99)
	- Dane-se as fases! :-D
* 2016-10-21 :
	- PyPy3.
	- Documentação.
	- encerramento (mesmo) desta fase. :-)
* 2016-10-20 :
	- Rápida refatoração para promover extensabilidade
	- Adicionar os metadados para o Agregador da tal "Confederação"
* 2016-10-19 :
	- Estabilização da interface REST
	- P/R :-)
	- encerramento desta fase.
* 2016-10-18 :
	- Incluindo o MSX Archive na search engine.
	- Encarando algumas decições de merd... digo.. infelizes que tomei. =P
	- Revamp das páginas HTML do W/S.
	- Testes de performance, profiling e benchmarking.
* 2016-10-17 :
	- Incluindo também um mirror do WoS.
	- Testes de performance e estabilidade
* 2016-10-16 :
	- net.lisias.retro.file.search.WS (que nome dou pra esta bosta?) .
	- Search engine de arquivos de FTP para o Asimov.
* 2016-10-16 :
	- http2.bash - Versão final.
	- Novas páginas HTML para P/R
* 2016-10-13 :
	- http2.bash
	- Outra implementação, mais avançada.
* 2016-10-12 :
	- http.bash
	- HTTP Server minimalista feito em bash
