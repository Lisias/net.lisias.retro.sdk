'''
Created on 23 de jan de 2017

@author: lisias
'''

import urllib.request

class Base(object):
	CONFEDERATION_URL = "http://home.lisias.net:8090"
	ORIGIN = "http://locahost"

	class Error(Exception):
		def __init__(self, msg):
			super().__init__(msg)

	def __init__(self, dataformat:str = ".txt"):
		self.version = None
		self.metadata = None
		self.dataformat = dataformat

	def metadata_get(self, archive:str, headers:dict = dict()):
		return self._do('{:s}/{:s}/metadata{:s}'.format(Base.CONFEDERATION_URL, archive, self.dataformat), 'GET', headers)

	def meta_get(self, metafield:str, headers:dict = dict()):
		return self._do('{:s}/{:s}{:s}'.format(Base.CONFEDERATION_URL, metafield, self.dataformat), 'GET', headers)

	def get(self, archive:str, entity:str, verb:str, parms:dict = dict(), headers:dict = dict(), klass = None):
		url = '{:s}/{:s}/{:s}/{:s}{:s}?{:s}'.format(
			Base.CONFEDERATION_URL,
			archive,
			entity,
			verb,
			self.dataformat,
			urllib.parse.urlencode(parms)
		)
		return self._do(url, 'GET', headers)

	def _do(self, url:str, method:str = 'GET', headers:dict = dict()) -> [bytes, urllib.response]:
		req = urllib.request.Request(url)
		req.method = method
		req.add_header('Origin', Base.ORIGIN)
		for i in headers.keys():
			req.add_header(i, headers[i])
		with urllib.request.urlopen(req) as r:
			return r.read(), r

import json
class RawJson(Base):
	'''
	Cliente Raw Json (ou qualquer outro formato suportado, menos Pickle)

	Vantagens:
		* Resiliência garantida em novas versões dos Providers
			- não tem o que dar errado, é tudo dicionário!

	Desvantagens:
		* Levemente mais lento de processar
		* Gera algum lixo para o Garbage Collector
		* Inconveniente, já que os Objetos viram dicionários e você deve garantir você mesmo a integridade
			e corretude dos dados (e dos nomes de campos).

	Quando usar:
		Quando não for desejado usar a biblioteca net.lisias.retro com os helpers para acessar a Confederação.
	'''

	def __init__(self):
		global VERSION, METADATA
		super().__init__(".json")

		self.version = self.meta_get('version') \
			if not self.version else self.version
		self.metadata = self.meta_get('metadata') \
			if not self.metadata else self.metadata

	def metadata_get(self, archive:str, headers:dict = dict()):
		body, response = super().metadata_get(archive, headers)
		return json.loads(str(body, encoding="utf-8"))

	def meta_get(self, metafield:str) -> dict :
		body, response = super().meta_get(metafield)
		return json.loads(str(body, encoding="utf-8"))

	def get(self, archive:str, entity:str, verb:str, parms:dict) -> dict:
		body, response = super().get(archive, entity, verb, parms)
		return json.loads(str(body, encoding="utf-8"))


import importlib.util
spam_spec = importlib.util.find_spec("net.lisias")
if spam_spec is not None:
	from net.lisias.Restful import Serializing
	import net.lisias.retro
	import net.lisias.retro.proxy

	class Json(Base):
		'''
		Cliente JSON (ou qualquer outro formato suportado, menos Pickle) com Deserialização.

		Vantagens:
			* Resiliência garantida em novas versões dos Providers
			* Conveniência ao usar os dados
				- Serão retornados objetos Python (com métodos auxiliares para conveniência e integridade)

		Desvantagens:
			* Sensivelmente mais lento de processar
			* Gera lixo extra para o Garbage Collector
			* Dependente da versão do EGG atualizada
				- Precisa instalar o EGG da Confederação no runtime
				- EGGs de versões anteriores terão os novos campos ignorados até atualização.
			* Inconveniência ao Deserializar os dados
				- Você precisa se preocupar em apontar a classe a ser usada na deserialização para cada Request
				- Atenção à documentação.

		Quando usar:
			Quase sempre, exceto quando a eficiência da transferência de dados for tão crítica que
			seja preferível ficar sem o serviço até atualização do EGG quando houver mudança de versão
			do Provider - o que, francamente falando, são casos limítrofes e possivelmente fora do escopo
			do Confederado comum.
		'''
		headers = {'Accept-Encoding':','.join(map(lambda x:str(x),Serializing.supported_encodings))}
		def __init__(self):
			global VERSION, METADATA
			super().__init__(".json")

			self.version = self.meta_get('version', klass=net.lisias.retro.Response.Version) \
				if not self.version else self.version
			self.metadata = self.meta_get('metadata', klass=net.lisias.retro.Response.Archives) \
				if not self.metadata else self.metadata

		def metadata_get(self, archive:str):
			body, response = super().metadata_get(archive, Json.headers)
			return Serializing.deserialize(body, response, net.lisias.retro.proxy.Response.Metadata)

		def meta_get(self, metafield:str, klass=None):
			body, response = super().meta_get(metafield, Json.headers)
			return Serializing.deserialize(body, response, klass)

		def get(self, archive:str, entity:str, verb:str, parms:dict, klass=None):
			body, response = super().get(archive, entity, verb, parms, Json.headers)
			return Serializing.deserialize(body, response, klass)

	class Pickle(Base):
		'''
		Cliente Pickle

		Vantagens:
			* Eficiência na comunicação
				- Menor uso de processamento de Strings
				- Menos lixo para o Garbage Collector coletar
			* Conveniência ao usar os dados
				- Serão objetos Python (com métodos auxiliares para conveniência e integridade)
			* Conveniência ao Deserializar os dados
				- Você não precisa se preocupar em apontar a classe a ser usada na deserialização.

		Desvantagens:
			* Dependencia crítica da versão do EGG atualizada
				- O código *vai quebrar* até atualizar o EGG para a nova versão.

		Quando usar:
			Apenas quando a eficiência da transferência de dados for tão crítica que
			seja preferível ficar sem o serviço até atualização do EGG quando houver mudança de versão
			do Provider.

			Nota do Lisias: Internamente, eu só uso pickle. Mas eu sou o Mestre dos Magos aqui, eu posso
			atualizar o que eu quiser quando eu bem entender.
		'''
		headers = {'Accept-Encoding':','.join(map(lambda x:str(x),Serializing.supported_encodings))}
		def __init__(self):
			global VERSION, METADATA
			super().__init__(".pickle")

			self.version = self.meta_get('version') \
				if not self.version else self.version
			self.metadata = self.meta_get('metadata') \
				if not self.metadata else self.metadata

		def metadata_get(self, archive:str):
			body, response = super().metadata_get(archive, Pickle.headers)
			return Serializing.deserialize(body, response)

		def meta_get(self, metafield:str):
			body, response = super().meta_get(metafield, Pickle.headers)
			return Serializing.deserialize(body, response)

		def get(self, archive:str, entity:str, verb:str, parms:dict):
			body, response = super().get(archive, entity, verb, parms, Pickle.headers)
			return Serializing.deserialize(body, response)
