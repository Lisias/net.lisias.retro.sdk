'''
Created on 24 de jan de 2017

@author: lisias
'''
import sys

import confederation.Client

import importlib.util
spam_spec = importlib.util.find_spec("net.lisias")
if spam_spec is not None:
	import net.lisias.retro.file.search

def str_to_class(classname:str) -> type:
	# https://stackoverflow.com/a/1176225
	try:
		identifier = eval(classname)
	except AttributeError:
		raise NameError("%s doesn't exist." % classname)
	if isinstance(identifier, type):
		return identifier
	raise TypeError("%s is not a class." % classname)

class Search:
	def __init__(self, client:confederation.Client.Base, archive:str):
		self.client = client
		# We can only check consistency when a fully serializable object is returned by the Client, otherwise we force a success and just throw the client to the lions (bad archive exceptions from the server)
		if archive not in (self.client.metadata.archives if hasattr(self.client.metadata, 'archives') else [archive]):
			raise Exception(archive + " não é suportado!")
		self.archive = archive

	def version(self):
		return self.client.version

	def search(self,
			keywords:set = set(),
			classes:set = set(),
			tags:set = set(),
			file_types:set = set(),
			fn_fragments:set = set(),
			option:str = None
			) -> set:
		params = dict()
		if option:
				params['option'] = option
		params['keywords'] = ','.join(keywords)
		params['classes'] = ','.join(classes)
		params['tags'] = ','.join(tags)
		params['file_types'] = ','.join(file_types)
		params['fn_fragments'] = ','.join(fn_fragments)
		return self.get(self.archive, 'files', 'search', params, 'net.lisias.retro.file.search.SearchResponse')

	def list(self, entity:str, prefix : str = None, count : int = None) -> set:
		params = dict()
		params['prefix'] = prefix
		params['count'] = count
		return self.get(self.archive, entity, 'list', params, 'net.lisias.retro.file.search.ListResponse')

	def get(self, archive:str, entity:str, verb:str, params:dict = dict(), classname = None):
		return self.client.get(self.archive, entity, verb, params, str_to_class(classname)) if isinstance(self.client, confederation.Client.Json) \
			else self.client.get(self.archive, entity, verb, params)



