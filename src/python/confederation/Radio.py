'''
Created on 13 de fev de 2017

@author: lisias
'''

import confederation.Client
import net.lisias.retro.file as FileInfo
import net.lisias.retro.radio

class WebRadio:
	ARCHIVE = 'webradio'
	_instanced = False

	class Json(confederation.Client.Json):
		def __init__(self):
			if WebRadio._instanced:
				raise confederation.Client.Base.Error("Singleton class. Do not instantiate again.")
			super().__init__()
			self.metadata = self.metadata_get(WebRadio.ARCHIVE, net.lisias.retro.radio.Response.Metadata) \
				if not self.metadata or not hasattr(self.metadata, 'data') else self.metadata
			WebRadio._instanced = True

		def get(self, entity:str, verb:str, parms:dict, klass=None):
			return super().get(WebRadio.ARCHIVE, entity, verb, {}, klass)

	class Pickle(confederation.Client.Pickle):
		ARCHIVE = 'webradio'
		def __init__(self):
			if WebRadio._instanced:
				raise confederation.Client.Base.Error("Singleton class. Do not instantiate again.")
			super().__init__()
			if not self.metadata or not hasattr(self.metadata, 'data'):
				self.metadata = self.metadata_get(WebRadio.ARCHIVE)
				self.metadata = net.lisias.retro.radio.Response.Metadata.rebuild(self.metadata.__dict__)
			WebRadio._instanced = True

		def get(self, entity:str, verb:str, parms:dict, klass=None):
			'''
			klass is ignored, as the Pickle format automatically uses the correct class
			'''
			return super().get(WebRadio.ARCHIVE, entity, verb, {})

class RemoteControl:
	class Error(Exception):
		def __init__(self, msg):
			super().__init__(msg)

	def __init__(self, client:confederation.Client.Base, channel_name:str):
		self.client = client

		self.channel = None
		for c in client.metadata.data.channels:
			if channel_name == c.name:
				self.channel = c
				break
		if not self.channel:
			raise RemoteControl.Error("Channel {:s} not found.".format(channel_name))

	def version(self):
		return self.client.version

	def playlist(self):
		return self.client.get(self.channel.symbols[0], 'list', {}, net.lisias.retro.radio.Response.Playlist)

	def current(self):
		return self.client.get(self.channel.symbols[0], "current", {}, net.lisias.retro.radio.Response.Current)

	def play(self, fileinfo:FileInfo):
		RemoteControl.Error("Não implementado ainda!")

	def remove_current(self):
		RemoteControl.Error("Não implementado ainda!")

	def next(self):
		return self.client.get(self.entity, "next", {}, net.lisias.retro.radio.Response.Current)

	def prev(self):
		return self.client.get(self.entity, "prev", {}, net.lisias.retro.radio.Response.Current)

	def balance(self):
		RemoteControl.Error("Não implementado ainda!")
