'''
Created on 23 de jan de 2017

@author: lisias

Exemplo de como fazer um request (ou vários) para a Confederação usando o pacote JSON do Python e um Cliente com abstração mínima (mas útil).

Se por um lado não exige o EGG da Confederação, por outro todos as Responses são deserializadas para dict's
ordinários - sem validação, sem checagem de tipo e, óbvio, sem a criação dos Objetos.
'''

from confederation.Client import Base, RawJson as JsonClient
Base.ORIGIN = "http://macmini.home.lisias.net"
from confederation.File import Search

client = JsonClient()

def look_metadata_for_search_archives() -> set:
	result = set()
	for d in client.metadata['archives']:
		archive_metadata = client.metadata_get(d)
		for verb in archive_metadata['verbs']:
			if 'search' == verb:
				if 'files' in archive_metadata['verbs']['search']:
					result.add(Search(client, d))
					continue
	return result

def search(
			archives : set,
			keywords:set = set(),
			classes:set = set(),
			tags:set = set(),
			file_types:set = set(),
			fn_fragments:set = set(),
		) -> set:
	def merge(l1, l2):
		yield from l1
		yield from l2

	result = list()
	count = 0
	for d in archives:
		r = d.search(keywords, classes, tags, file_types, fn_fragments, 'SHORT')
		if r['error']:
			print(r['error_msg'])
		else:
			#print(r.success_msg)
			result = list(merge(result, r['files']))
			count += r['cardinality']
	return list(result), count

def pretty_print(files:list):
	for f in files:
		print(f['filename'])
		if 'description' in f:
			print("\t" + f['description'].split('\n')[0])
		print("\t" + f['url'])

if __name__ == '__main__':
	print("{:s} {:s}".format(client.version['app'], client.version['code']))
	search_engines = look_metadata_for_search_archives()
	x, count = search(search_engines, keywords=set(['love']))
	pretty_print(x)
	print ("{:d} of {:d} files.".format(len(x), count))
