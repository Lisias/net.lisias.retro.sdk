'''
Created on 23 de jan de 2017

@author: lisias

Exemplo de como fazer um request (ou vários) para a Confederação usando a deserialização (com compactação)
automática para objetos Python.

Exige o EGG da confederção instalado - o que pode ser um aborrecimento, uma vez que o EGG instala também
todas as dependências que você possivelmente não usará num cliente console.
'''

from confederation.Client import Base, Json as Client # Change to Pickle for the best performance possible!
Base.ORIGIN = "http://macmini.home.lisias.net"
from confederation.File import Search

client = Client()

def look_metadata_for_search_archives() -> set:
	result = set()
	for d in client.metadata.archives:
		archive_metadata = client.metadata_get(d)
		for verb in archive_metadata.verbs:
			if 'search' == verb:
				if 'files' in archive_metadata.verbs['search']:
					result.add(Search(client, d))
					continue
	return result

def search(
			archives:set,
			keywords:set = set(),
			classes:set = set(),
			tags:set = set(),
			file_types:set = set(),
			fn_fragments:set = set(),
		) -> set:
	def merge(l1, l2):
		yield from l1
		yield from l2

	result = list()
	count = 0
	for d in archives:
		r = d.search(keywords, classes, tags, file_types, fn_fragments, 'SHORT')
		if r.error:
			print(r.error_msg)
		else:
			#print(r.success_msg)
			result = merge(result, r.files)
			count += r.cardinality
	return list(result), count

def pretty_print(files:set):
	for f in files:
		print(f.filename)
		if hasattr(f, 'description'):
			print("\t" + f.description.split('\n')[0])
		print("\t" + f.url)

if __name__ == '__main__':
	print("{:s} {:s}".format(client.version.app, client.version.code))
	search_engines = look_metadata_for_search_archives()
	x, count = search(search_engines, keywords=set(['love']))
	pretty_print(x)
	print ("{:d} of {:d} files.".format(len(x), count))
