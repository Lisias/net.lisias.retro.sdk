'''
Created on 9 de fev de 2017

@author: lisias

'''
#!/usr/bin/env python

import asyncio
import sys, io

HOST = 'home.lisias.net'
GET = '/AmigaMod'
PORT = 8000

@asyncio.coroutine
def data_inputstream_process():
    reader, writer = yield from asyncio.open_connection(HOST, PORT)
    h = "GET %s HTTP/1.0\r\nHost: %s\r\nUser-Agent:%s\r\nIcy-MetaData:%s\r\nRange:%s\r\n\r\n" % (GET, HOST,"VLC/2.0.5 LibVLC/2.0.5", "0", "bytes=0-")
    print (h)
    writer.write(bytes(h, encoding="ascii"))

    while True:
        b = yield from reader.read(256)
        print(b)
    writer.close()

if __name__ == '__main__':

    loop = asyncio.get_event_loop()
    loop.run_until_complete(data_inputstream_process())

    sys.exit(0)
