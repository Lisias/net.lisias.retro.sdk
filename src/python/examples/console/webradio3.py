'''
Created on 9 de fev de 2017

@author: lisias

Dependências:

MacOS:
	port install portaudio lame
	pip install --global-option='build_ext' --global-option='-I/opt/local/include' --global-option='-L/usr/opt/lib' pyaudio

Linux:
	apt-get install portaudio lame
ou
	yum install portaudio lame

	pip install pyaudio

No windows vai precisar de uma modificaçãozinha ou outra.

'''
#!/usr/bin/env python

import asyncio, struct
import sys, io, os, threading, queue
from subprocess import Popen, PIPE
import subprocess
from urllib.parse import urlparse
import pyaudio

from confederation.Radio import WebRadio, RemoteControl

CONFEDERATION_URL = 'home.lisias.net'
RADIO_CHANNEL = '??'
LAME_BINS = ['/opt/local/bin/lame', '/usr/local/bin/lame', '/usr/bin/lame']

# Configure aqui se você deseja usar o protocolo de comunicação binário (Pickle) ou textual (JSON).
BINARY_PROTOCOL = True

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

class Mp3DecodingStream():
	def __init__(self):
		noWindow = 0x8000000 if (os.name=="nt") else 0

		cline = ['which', 'lame']
		p = subprocess.Popen(cline, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,creationflags=noWindow)
		self.lame, err = p.communicate()
		self.lame = str(self.lame, encoding='utf-8')
		if b'' != err or '' == self.lame:
			self.lame = None
			eprint("Não consegui identificar o lame, erro no sistema {:s}. procurando por um dos LAME_BINS (ver fonte).".format(str(err, encoding='utf-8')))
			for l in LAME_BINS:
				if os.path.exists(l):
					self.lame = l
					break
			if self.lame:
				eprint("Usando lame em {:s}".format(self.lame))
			else:
				raise Exception("Binário do LAME não foi achado. Sem condições de continuar...")

		cline = [self.lame, "--quiet", "--decode", "--mp3input", "-", "-"]
		self.decoder_process = Popen(cline, shell=False, stdout=PIPE, stdin=PIPE, stderr=sys.stderr, creationflags=noWindow)
		self.read = self.decoder_process.stdout.read
		self.write = self.decoder_process.stdin.write

	def configure(self, audioinfo : dict):
		self.audioinfo = audioinfo

class AudioPlayer():
	class Producer(threading.Thread):
		def __init__(self, queue, decodestream, lote : int):
			super().__init__()
			self.queue = queue
			self.decodestream = decodestream
			self.lote = lote

		def run(self):
			while True:
				self.queue.put(self.decodestream.read(self.lote))

	class Consumer(threading.Thread):
		def __init__(self, queue, audiostream):
			super().__init__()
			self.queue = queue
			self.audiostream = audiostream

		def run(self):
			while True:
				message = self.queue.get()
				self.audiostream.write(message)
				self.queue.task_done()

	def __init__(self, decodestream):
		super().__init__()
		self.decodestream = decodestream
		self.write = self.decodestream.write
		self.device = pyaudio.PyAudio()
		self.queue = queue.Queue(4)

	def configure(self, audioinfo : dict):
		self.audioinfo = audioinfo
		self.decodestream.configure(audioinfo)
		self.audiostream = self.device.open(
				format=pyaudio.paInt16,
				channels=self.audioinfo['channels'],
				rate=self.audioinfo['samplerate'],
				output=True)
		self.lote = self.audioinfo['samplerate'] * self.audioinfo['channels'] * 2 # sizeof(pyaudio.paInt16)

		self.producer = AudioPlayer.Producer(self.queue, self.decodestream, self.lote)
		self.consumer = AudioPlayer.Consumer(self.queue, self.audiostream)
		self.producer.start()
		threading.Timer(1, AudioPlayer.wait_queue, (self,)).start()

	def wait_queue(self):
		if self.queue.full():
			self.consumer.start()
		else:
			threading.Timer(1, AudioPlayer.wait_queue, (self,)).start()

class IcecastInputStream:
	class Error(Exception):
		def __init__(self, instance):
			self.httpr, self.headers, self.body = instance.http_result, instance.headers, instance.error_body

		def __str__(self):
			return self.httpr + "\n" \
				+ "\n".join(["{:s}:{:s}".format(k,v) for k,v in self.headers.items()]) + "\n" \
				+ self.body

	class BufferedStream:
		def __init__(self, maximum_buffer_size : int = 65536):
			self.buffer = bytes()
			self.logical_pos = 0
			self.physical_pos = 0
			self.max = maximum_buffer_size

		def tell(self):
			return self.logical_pos

		def seek(self, offset:int, whence:int = 0):
			print (offset, whence)
			if 2 == whence:
				raise io.UnsupportedOperation("Seek from the end of file not supported!")
			elif 1 == whence:
				if (self.physical_pos + offset) > len(self.buffer):
					raise io.UnsupportedOperation("Seek beyond local buffer not supported!")
				self.physical_pos += offset
				self.logical_pos += offset
			else:
				if (self.logical_pos - offset + self.physical_pos) > len(self.buffer):
					raise io.UnsupportedOperation("Seek beyond local buffer not supported!")
				self.physical_pos += offset
				self.logical_pos += offset

		def read(self, size : int):
			l = len(self.buffer)

			if size > (l - self.physical_pos):
				size = l - self.physical_pos

			i = self.physical_pos
			self.physical_pos += size
			r = self.buffer[i:self.physical_pos]

			self.logical_pos += size
			return r

		def readline(self):
			try:
				i = self.buffer[self.physical_pos:].index(b'\r\n')+self.physical_pos
			except:
				i = len(self.buffer)-1

			r = self.buffer[self.physical_pos:i]
			self.physical_pos += (i-self.physical_pos)+2
			self.logical_pos += (i-self.physical_pos)+2
			return str(r, encoding='ascii')

		def inject(self, data : bytes):
			self.buffer = data + self.buffer[self.physical_pos:]
			self.physical_pos = 0

		def write(self, data : bytes):
			self.buffer = self.buffer + data
			if (len(self.buffer)) > self.max and 0 != self.physical_pos:
				l = len(data)
				l = l if (self.physical_pos >= l) else self.physical_pos
				self.buffer = self.buffer[l:]
				self.physical_pos -= l

		def size(self):
			return len(self.buffer)

	def __init__(self, audiostream, play_notifier = None, metadata_notifier = None):
		self.stream = IcecastInputStream.BufferedStream()
		self.outputaudiostream = audiostream
		self.current_state = IcecastInputStream._state_http_result
		self.play_notifier = play_notifier
		self.metadata_notifier = metadata_notifier
		self.http_result = ''
		self.headers = dict()
		self.error_body = ''
		self.audioinfo = dict()

	def _state_http_result(self):
		eprint("_state_http_result")
		data = self.stream.readline()

		self.http_result = data
		if 'HTTP/1.0 200 OK' == data:
			return IcecastInputStream._state_reading_headers

		self.error_body += data
		return IcecastInputStream._state_reading_error_headers

	def _state_reading_error_headers(self):
		eprint("_state_reading_error_headers")
		data = self.stream.readline()

		if ':' in data:
			key, value = data.split(':', 1)
			self.headers[key] = value.strip()
		elif "" == data:
			return IcecastInputStream._state_reading_error_body

		return IcecastInputStream._state_reading_error_headers

	def _state_reading_error_body(self):
		eprint("_state_reading_error_body")
		data = self.stream.readline()

		self.error_body += data
		if "" == data:
			raise IcecastInputStream.Error(self)

		return IcecastInputStream._state_reading_error_body

	def _state_reading_headers(self):
		eprint("_state_reading_headers")
		data = self.stream.readline()

		if ':' in data:
			key, value = data.split(':', 1)
			self.headers[key] = value.strip()
		elif "" == data:
			return IcecastInputStream._state_received_headers_init_player

		return IcecastInputStream._state_reading_headers

	def _state_received_headers_init_player(self):
		eprint("_state_received_headers_init_player")
		self.audioinfo['bitrate'] = int(self.headers['icy-br'])
		for i in self.headers['ice-audio-info'].split(';'):
			key, value = i.split('=', 1)
			self.audioinfo[key] = int(value)
		self.station_name = self.headers['icy-name']
		self.station_description = self.headers['icy-description']
		pub = self.headers['icy-pub']
		try: # Metaint is interval in bytes im which metadata is sent on the stream! http://stackoverflow.com/questions/14540380/title-of-current-icecast-streamed-song
			self.audioinfo['metaint'] = int(self.headers['icy-metaint'])
		except:
			self.audioinfo['metaint'] = 0
		# fake a mp3 envelope and inject into reader!
		return IcecastInputStream._state_bufferize_before_play_music

	def _state_bufferize_before_play_music(self):
		eprint("_state_bufferize_before_play_music", self.stream.size())
		return IcecastInputStream._state_start_play_music if self.stream.size() > 2*self.audioinfo['metaint'] else IcecastInputStream._state_bufferize_before_play_music

	def _state_start_play_music(self):
		eprint("_state_start_play_music")
		self.outputaudiostream.configure(self.audioinfo)
		if self.play_notifier:
			threading.Thread(target=self.play_notifier).start()
		self.audio_size = 0
		return IcecastInputStream._state_play_music

	def _state_play_music(self):
		size = self.audioinfo['metaint'] - self.audio_size
		if 0 == size:
			self.audio_size = 0
			self.metadata = bytes()
			self.metadata_size = self.stream.read(1)[0]* 16
			return IcecastInputStream._state_parse_metadata
		self.audio_size += self.outputaudiostream.write(self.stream.read(size))
		return IcecastInputStream._state_play_music

	def _state_parse_metadata(self):
		self.metadata += self.stream.read(self.metadata_size - len(self.metadata))
		if len(self.metadata) == self.metadata_size:
			if self.metadata_notifier and 0 != self.metadata_size:
				r = dict()
				data = str(self.metadata, encoding="utf-8")
				for item in [i for i in data.split(';') if -1 != i.find('=')]:
					k,v = item.split("=")
					r[k.strip()] = v.strip()
				threading.Thread(target=self.metadata_notifier, args=(r,)).start()
			return IcecastInputStream._state_play_music
		return IcecastInputStream._state_parse_metadata

	def notify_data(self):
		self.current_state = self.current_state(self)

	def write(self, buffer:bytes):
		self.stream.write(buffer)

@asyncio.coroutine
def data_inputstream_process(data_consumer, host, port, channel):
	reader, writer = yield from asyncio.open_connection(host, port)
	writer.write(bytes("GET %s HTTP/1.0\r\nHost: %s\r\nUser-Agent:%s\r\nIcy-MetaData:%s\r\nRange:%s\r\n\r\n" % (channel, host, "net.lisias.retro.radio Client", "1", "bytes=0-"), encoding="ascii"))

	while True:
		b = yield from reader.read(1024)
		data_consumer.write(b)
		data_consumer.notify_data()

	writer.close()

if __name__ == '__main__':

	client = (WebRadio.Pickle if BINARY_PROTOCOL else WebRadio.Json)()

	print("{:s} {:s}".format(client.version.app, client.version.code))

	try:
		service = RemoteControl(client, RADIO_CHANNEL)
	except:
		print("Canal {:s} inexistente! Pegando o primeiro que aparecer!".format(RADIO_CHANNEL))
		for v in client.metadata.data.channels:
			service = RemoteControl(client, v.name)
			print (v.name + " selecionado.")
			break

	station = [ s for s in client.metadata.data.stations if 'PUBLIC' == s.stream.name][0]

	def notify_playing():
		print ("playing!!")
		playlist = service.playlist().entries
		playlist.sort(key = lambda entry: entry.pos)
		for music in playlist:
			print(str(music))
		response = service.current()
		print ("Tocando agora:" + str(response.current))

	def notify_metadata(d):
		for k, v in d.items():
			eprint (k, v)
		try:
			response = service.current()
			print ("Tocando agora:" + str(response.current))
		except:
			# For some reason, the first request are returning "http.client.IncompleteRead: IncompleteRead(208 bytes read, 900 more expected)"
			# On the server side, everything appears to work fine, with the message "GET /webradio/AmigaMod/current.python HTTP/1.1" 200 393 0.992387"
			# Well, I will ignore the problem for now. Only the first request is kaput anyway...
			pass

	mp3 = Mp3DecodingStream()
	pya = AudioPlayer(mp3)
	icy = IcecastInputStream(pya, notify_playing, notify_metadata)

	loop = asyncio.get_event_loop()
	try:
		url = urlparse(station.url)
		loop.run_until_complete(data_inputstream_process(icy, url.hostname, url.port, url.path))
	except Exception as e:
		print(str(e))
		raise e

	sys.exit(0)
