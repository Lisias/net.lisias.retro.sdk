'''
Created on 23 de jan de 2017

@author: lisias

Exemplo de como fazer um request (ou vários) para a Confederação por força bruta.

Para os die-hards que acham Orientação à Objetos uma frescura do carvalho. :-)
'''

import json, urllib.request
import gzip, zlib


ORIGIN = "http://macmini.home.lisias.net"
CONFEDERATION_URL = "http://home.lisias.net:8090"
HEADERS = {'Accept-Encoding' : 'gzip,deflate'}

def do_request(url:str, method:str = 'GET') -> [bytes, urllib.response]:
	req = urllib.request.Request(url)
	req.method = method
	req.add_header('Origin', ORIGIN)
	for i in HEADERS.keys():
		req.add_header(i, HEADERS[i])
	with urllib.request.urlopen(req) as r:
		if 'gzip' == r.info()['Content-Encoding']:
			return gzip.decompress(r.read()), r
		elif 'gzip' == r.info()['Content-Encoding']:
			return zlib.decompress(r.read()), r
		else:
			return r.read(), r

def get_version():
	return json.loads(
			str(do_request('{:s}/version.json'.format(CONFEDERATION_URL))[0],encoding='utf-8')
		)

def get_metadata():
	return json.loads(
			str(do_request('{:s}/metadata.json'.format(CONFEDERATION_URL))[0],encoding='utf-8')
		)

def get_archive_metadata(archive:str):
	return json.loads(
			str(do_request('{:s}/{:s}/metadata.json'.format(CONFEDERATION_URL, archive))[0],encoding='utf-8')
		)


def parse_metadata_for_search_archives(metadata:dict):
	result = set()
	for archive in metadata['archives']:
		archive_metadata = get_archive_metadata(archive)
		for verb in archive_metadata['verbs']:
			if 'search' == verb:
				if 'files' in archive_metadata['verbs']['search']:
					result.add(archive)
					continue
	return result

def get_search(archive:str, parms:dict = dict()):
	p = dict({'option':'SHORT'})
	for k in parms.keys():
		p[k] = ','.join(parms[k])
	url = '{:s}/{:s}/files/search.json?{:s}'.format(
		CONFEDERATION_URL,
		archive,
		urllib.parse.urlencode(p)
	)
	return json.loads(
			str(do_request(url)[0],encoding='utf-8')
		)

def search(
			archives:set,
			parms:dict = dict(),
		) -> set:
	def merge(l1, l2):
		yield from l1
		yield from l2
	result = list()
	count = 0
	for d in archives:
		r = get_search(d, parms)
		if r['error']:
			print(r['error_msg'])
		else:
			#print(r.success_msg)
			result = merge(result, r['files'])
			count += r['cardinality']
	return list(result), count

def pretty_print(files:list):
	for f in files:
		print(f['filename'])
		if 'description' in f:
			print("\t" + f['description'].split('\n')[0])
		print("\t" + f['url'])

if __name__ == '__main__':
	version = get_version()
	print("{:s} {:s}".format(version['app'], version['code']))
	metadata = get_metadata()
	archives = parse_metadata_for_search_archives(metadata)
	x, count = search( archives, {'keywords':set(['love'])} )
	pretty_print(x)
	print ("{:d} of {:d} files.".format(len(x), count))
