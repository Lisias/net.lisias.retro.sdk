'''
Created on 9 de fev de 2017

@author: lisias

Dependências:

MacOS:
	port install portaudio
	pip install --global-option='build_ext' --global-option='-I/opt/local/include' --global-option='-L/usr/opt/lib' pyaudio

Linux:
	apt-get install portaudio portaudio-dev
ou
	yum install portaudio portaudio-devel

	pip install pyaudio

Para o OLPC, ver arquido docs/clients/console.md#webradio2e3

No windows vai precisar de uma modificaçãozinha ou outra.

'''

#!/usr/bin/env python
import socket
import sys
import threading
import queue
import pyaudio

class Player(threading.Thread):

	def __init__(self, messageQueue, audioinfo):
		threading.Thread.__init__(self)

		self.metaint = audioinfo

		self.messageQueue = messageQueue
		self.device = pyaudio.PyAudio()
		self.buffer = bytes()
		self.stream = self.device.open(
				format=pyaudio.paInt16,
				channels=2,
				rate=44100,
				output=True)

	def sendPCM(self):
		print("Buffer length: " + str(len(self.buffer)))
		if len(self.buffer) > self.metaint + 255:
			pcmData = self.buffer[:self.metaint]
			self.stream.write(pcmData)
			self.buffer = self.buffer[self.metaint:]
			print ("New buffer length 1: " + str(len(self.buffer)))
			metaDataLength = ord(self.buffer[:1]) * 16
			print ("Metadata length: " + str(metaDataLength))
			self.buffer = self.buffer[1:]
			print ("New buffer length 2: " + str(len(self.buffer)))
			metaData = self.buffer[:metaDataLength]
			print (len(metaData))
			self.buffer = self.buffer[metaDataLength:]
			print ("New buffer length 3: " + str(len(self.buffer)))


	def run(self):
		self.sendPCM()
		while True:
			message = self.messageQueue.get()
			if message: self.buffer += message
			self.sendPCM()
			self.messageQueue.task_done()

def getResponseHeaders(socket):
	data = socket.recv(1024)

	while not "\r\n\r\n" in str(data, encoding='ascii'):
		data = data + socket.recv(1024)

	return data

def getHeaders(response):
	headers = {}
	response = str(response, encoding='ascii')
	for line in response.splitlines():
		if line == '\r\n':
			break # end of headers
		if ':' in line:
			key, value = line.split(':', 1)
			headers[key] = value

	return headers

if __name__ == '__main__':

	HOST = 'home.lisias.net'
	GET = '/AmigaMod'
	PORT = 8000

	#create an INET, STREAMing socket
	client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	client_socket.connect((HOST, PORT))
	client_socket.send(bytes("GET %s HTTP/1.0\r\nHost: %s\r\nUser-Agent:%s\r\nIcy-MetaData:%s\r\nRange:%s\r\n\r\n" % (GET, HOST,"VLC/2.0.5 LibVLC/2.0.5", "0", "bytes=0-"), encoding="ascii"))

	responseHeaders = getResponseHeaders(client_socket)
	headers = getHeaders(responseHeaders)
	metaint = int(headers['icy-metaint']) if 'icy-metaint' in headers else 0
	br = int(headers['icy-br'])
	audioinfo = dict()
	for i in headers['ice-audio-info'].split(';'):
		k,v = i.split('=')
		audioinfo[k] = v

	queue = queue.Queue()

	player = Player(queue, metaint)
	player.daemon = True
	player.start()

	while True:
		queue.put(client_socket.recv(4096))

	client_socket.close()
	sys.exit(0)
