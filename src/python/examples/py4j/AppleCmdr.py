'''
Created on Nov 29, 2017

@author: lisias
'''

import os
from py4j.java_gateway import JavaGateway, GatewayParameters

PROJECT_ROOT = os.environ['PROJECT_LOC']
DATA = os.path.join(PROJECT_ROOT, 'data/a2/disks')
gateway = JavaGateway(gateway_parameters=GatewayParameters(auto_convert=True))

GET_FILE_ATRIBUTES_DATA={
		'DOS 3.3' : ['locked?', 'file_type', 'name', 'size', 'unk0', 'unk1', 'unk3'],
		'CP/M' : ['name', 'extension', 'size', 'user', 'unk0', 'unk1'],
		'Pascal' : ['changed_at', 'unk0', 'unk1', 'size', 'file_type', 'name', 'unk2', 'unk3'],
		'ProDOS' : ['locked?', 'name', 'unk0', 'access_rights', 'file_type', 'unk1', 'unk2', 'creation_at', 'changed_at', 'size', 'A', 'unk3', 'unk4', 'storage_type', 'status', 'unk5', 'unk6'],

	}
def get_file_atributes(disk_format:str, file) -> dict:
	r = dict()
	atrib_data = file.getFileColumnData(gateway.jvm.com.webcodepro.applecommander.storage.FormattedDisk.FILE_DISPLAY_DETAIL)
	atrib_name = GET_FILE_ATRIBUTES_DATA[disk_format] if disk_format in GET_FILE_ATRIBUTES_DATA else [str(n) for n in range(len(atrib_data))]

	for i in zip(atrib_name, atrib_data):
		r[i[0]] = i[1]

	return r

def show_files(disk_format:str, files : list, ident : int = 0) -> None:
	for f in files:
		d = f.getFileColumnData(gateway.jvm.com.webcodepro.applecommander.storage.FormattedDisk.FILE_DISPLAY_DETAIL)
		print("%s%s%s \t %s \t %s%s - %s" % (
			'\t'*ident,
			'*' if f.isLocked() else ' ',
			f.getFilename(),
			f.getFiletype(),
			'<DIR>' if f.isDirectory() else '',
			'<del>' if f.isDeleted() else '',
			':'.join(d)
			))
		for a,v in get_file_atributes(disk_format, f).items():
			print("%s%s : %s" % (
				'\t'*(1+ident),
					a, v
				) )

		if f.isDirectory() and not f.isDeleted():
			show_files(disk_format, f.getFiles(), ident+1)


def show_disk( d : gateway.jvm.com.webcodepro.applecommander.storage.Disk ) -> None:
	fds = d.getFormattedDisks()
	for fd in fds:
		disk_format = None
		for i in fd.getDiskInformation():
			if 'Disk Format' == i.getLabel():
				disk_format = i.getValue()
			print('\t%s : %s' % (i.getLabel(), i.getValue()))
		show_files(disk_format, fd.getFiles(), 2)

if __name__ == '__main__':
	Disk = gateway.jvm.com.webcodepro.applecommander.storage.Disk
	for fn in os.listdir(path=DATA):
		print(fn)
		pfn = os.path.join(DATA, fn)
		d = Disk(pfn)
		show_disk(d)
		print()
