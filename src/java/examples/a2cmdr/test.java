package examples.a2cmdr;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.webcodepro.applecommander.storage.Disk;
import com.webcodepro.applecommander.storage.DiskException;
import com.webcodepro.applecommander.storage.FileEntry;
import com.webcodepro.applecommander.storage.FormattedDisk;

public class test {

	public static void main(String[] args) {
		final File data = new File(System.getenv().get("DATA"));

		for (File f : data.listFiles()) {
			System.out.println(f.getName());
			show_disk(f.getAbsoluteFile());
			System.out.println();
		}

	}

	private static void show_disk(final File diskfile) {
		try {
			final Disk d = new Disk(diskfile.getAbsolutePath());
			final FormattedDisk[] fds = d.getFormattedDisks();
			for (FormattedDisk fd : fds) {
				String disk_format = null;
				for (FormattedDisk.DiskInformation i : fd.getDiskInformation()) {
					if ("Disk Format".equals(i.getLabel())) disk_format = i.getLabel();
					System.out.println(String.format("\t%s : %s", i.getLabel(), i.getValue()));
					show_files(disk_format, fd.getFiles(), 2);
				}
			}
		} catch (DiskException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void show_files(final String disk_format, final List<FileEntry> files, final int i) {
		// TODO Auto-generated method stub

	}

}
