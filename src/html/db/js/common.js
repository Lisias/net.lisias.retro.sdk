JSONP.init({
	error: function(ex){
		const v = document.getElementById('version')
		while (v.firstChild) {
        	v.removeChild(v.firstChild);
		}
      	p = document.createElement('span');
		p.innerHTML = "err:" + ex.event.toString();
		v.appendChild(p);
	}
});

function update_version() {
	JSONP.get(REQUEST('version'), {}, function(data) {
		const v = document.getElementById('version')
		while (v.firstChild) {
	    	v.removeChild(v.firstChild);
		}
		p = document.createElement('span');
		p.innerHTML = data.app + " " + data.code;
		v.appendChild(p);
	});
}
update_version();
