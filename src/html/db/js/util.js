function escapeHtml(text) {
		'use strict';
		return text.replace(/[\"&'\/<>\n]/g, function (a) {
				return {
						'"': '&quot;', '&': '&amp;', "'": '&apos;',
						'/': '&#47;',  '<': '&lt;',  '>': '&gt;',
						'\n': "<br/>"
				}[a];
		});
}

function stripForHtml(text) {
		'use strict';
		return text.replace(/[\"&'\/<>]/g, function (a) {
				return {
						'"': '', '&': '', "": '',
						'/': '', '<': '', '': ';'
				}[a];
		});
}
