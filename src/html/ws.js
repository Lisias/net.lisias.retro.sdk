const DEBUG = true; // Change to false on deploy
const LOCAL = true; // Change to false on deploy
const WS_ROOT = DEBUG && LOCAL
					? "http://localhost:8090"
					: LOCAL
					? "http://rpi-0-A.cluster.retro.lisias.net:8090"
					: "http://home.lisias.net:8090"
				;

function select_logo() {
	if (DEBUG && LOCAL) return; // Don't hit the server while using it locally for development
	const img = new Image();
	img.onload = function() {
		document.body.style.backgroundImage = "url(" + this.src + ")";
	}
	img.src = "http://static.lisias.net/img/L.jpg";
}
