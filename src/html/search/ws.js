require(["./js/jsonp.js"]);

function REQUEST(u) {
	return WS_ROOT + "/" + u + ".jsonp";
}
function REQUESTAPP(u) {
	return WS_ROOT + "/" + APP_ROOT + "/" + u + ".jsonp";
}
function ACLAPP(u) {
	return PROVIDER_ROOT() + "/acl/webradio/" + u + ".jsonp";
}
function PLAYERAPP(u) {
	return PROVIDER_ROOT() + "/webradio/" + u + ".jsonp";
}
function PLAYERCHANNELAPP(u) {
	return PROVIDER_ROOT() + "/webradio/" + CHANNEL + "/" + u + ".json";
}
var __private_provider = null;
function PROVIDER_ROOT() {
	return __private_provider.url;
}

function fetch_provider_from_proxy_because_cors_sucks_and_dont_allow_me_to_redirect_services(archive, entity, bootstrap) {
	JSONP.get(REQUEST('metadata') + '?archive=' + archive + ";entity=" + entity
			, {}
			, function(data) {
				__private_provider = data.providers[0];
				if ('/' == __private_provider.url.slice(-1)) {
					__private_provider.url = __private_provider.url.slice(0,-1);
				}
				bootstrap();
			}
		);
}
