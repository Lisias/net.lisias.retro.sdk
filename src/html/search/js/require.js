var require = (function () {
var _required = {};
return (function (url, callback) {
    if (typeof url == 'object') {
        // We've (hopefully) got an array: time to chain!
        if (url.length > 1) {
            // Load the nth file as soon as everything up to the
            // n-1th one is done.
            require(url.slice(0,url.length-1), function () {
                require(url[url.length-1], callback);
            });
        } else if (url.length == 1) {
            require(url[0], callback);
        }
        return;
    }
    if (typeof _required[url] == 'undefined') {
        // Haven't loaded this URL yet; gogogo!
        _required[url] = [];

        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.async = true;
        script.onload = function () {
            console.log("script " + url + " loaded.");
            
            for (var i = 0; i < _required[url].length; i++) {
            	_required[url][i].call();
            }

            _required[url] = true;
        };

        const firstsScript = document.getElementsByTagName('head')[0];
        firstsScript.parentNode.insertBefore(script, firstsScript);
    } else if (typeof _required[url] == 'boolean') {
        // We already loaded the thing, so go ahead.
        if (callback) { callback.call(); }
        return;
    }

    if (callback) { _required[url].push(callback); }
});
})();

// http://stackoverflow.com/a/2122234
// http://stackoverflow.com/a/950146
// http://stackoverflow.com/a/31282622