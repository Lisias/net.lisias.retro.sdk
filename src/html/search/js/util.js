//require(["./js/lz-string.js"]);

navigator.webkitTemporaryStorage.queryUsageAndQuota (
    function(usedBytes, grantedBytes) {
	const NEEDED_SPACE = 512 *1024 *1024;
        console.log('we are using ', usedBytes, ' of ', grantedBytes, 'bytes');
//         if ( (grantedBytes - usedBytes) < NEEDED_SPACE) {
// 		navigator.webkitPersistentStorage.requestQuota (
// 		    	NEEDED_SPACE + usedBytes, function(grantedBytes) {
// 			window.requestFileSystem(PERSISTENT, grantedBytes, onInitFs, errorHandler);
// 		    }, function(e) { console.log('Error', e); }
// 		);
// 	}
    },
    function(e) { console.log('Error', e);  }
);

function escapeHtml(text) {
		'use strict';
		return text.replace(/[\"&'\/<>]/g, function (a) {
				return {
						'"': '&quot;', '&': '&amp;', "'": '&apos;',
						'/': '&#47;',  '<': '&lt;',  '>': '&gt;'
				}[a];
		});
}

function stripForHtml(text) {
		'use strict';
		return text.replace(/[\"&'\/<>]/g, function (a) {
				return {
						'"': '', '&': '', "": '',
						'/': '', '<': '', '': ';'
				}[a];
		});
}
