require(["./js/util.js", "./js/webradio.js"], webradio_thick_init);

function webradio_thick_init() {
}

function render_playlist(entry) {
	const r =
		"<b>" + entry.pos + '</b>'
		+ (
			entry.author && entry.title
				? " " + entry.author + " : " + entry.title
			: entry.author || entry.title
				? " " + (entry.author ? entry.author : entry.title)
			: ""
		)
		+ "<br/>"
		;
	return r;
}

function render_filedata_with_player(filedata) {
	const d = (filedata.is_content && undefined != filedata.owner) ? filedata.owner.dirs.slice() : filedata.dirs.slice();
	if (filedata.is_content && undefined != filedata.owner) {
		d.push(filedata.owner.filename);
		d.concat(filedata.dirs);
	}
	const dir 	= d.join('/');
	const name 	= (undefined != filedata.alias && filedata.alias) ? filedata.alias : filedata.filename;
	const code = stripForHtml(filedata.url);
	FILEDATA[code] = filedata;
	const is_music = is_file_music[APP_ROOT](filedata);

	const r = "<div style=\"position: relative;\">"
				+ "<a href=\"" + filedata.url + '"'
						+ (filedata.description ? " title = \"" + escapeHtml(filedata.description) + '"' : "")
						+ '">'
					+ "<img style=\"position: absolute; top: 50%; margin-top: -2ch; height: inherit; max-width: 4ch; width: auto;\" src=\""
						+ (	filedata.is_content ? "./img/icon.file.png"
							: filedata.is_cabinet ? "./img/icon.floppydisk.png"
							: is_music ? "./img/icon.music.png"
							: "./img/icon.unknown.png"
						) + "\"/>"
				+ "</a>"
				+ "<span style=\"margin-left: 5ch; display: block;\">"
					+ dir + "&nbsp;:<br/>"
					+ "&nbsp;&nbsp;&nbsp;&nbsp;"
						+ "<a href=\"" + filedata.url + '"'
						+ (filedata.description ? " title = \"" + escapeHtml(filedata.description) + '"' : "")
						+ '">'
						+ name
						+ "</a>"
						+ (filedata.description ? "&nbsp;&#x2139;" : "")
					+ "&nbsp;<br/>"
					+ (
						is_music
							? "<button type='button' onclick=\"btn_play_music('" + code + "')\" title='Custo: 10 Créditos'><b>&nbsp;&GT;&nbsp;</b></button>"
							: ""
					)
				+ "</span>"
		+ "</div>"
	;
	return r;
}

function btn_DoItPlayer() {
	FILEDATA = {};
	btn_doIt(render_filedata_with_player);
}
