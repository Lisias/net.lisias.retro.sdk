require(['./js/util.js', './js/common.js', './js/cache.thick.js'], common_thick_init);

function common_thick_init() {
}

var keywords_input_counter = 0;
const keywords_input_list = [];
function btn_add_keyword() {
	keywords_input_counter++;
	const newname = "keywords_input_" + keywords_input_counter;

	const r = document.getElementById('keywords_boxes');
	p = document.createElement('input');
	p.setAttribute("id", newname)
	r.appendChild(p);
	const input = document.getElementById(newname);
	create_input_component(input, KEYWORDS);
	keywords_input_list.push(input);
	input.focus();
}

var class_input_counter = 0;
const class_input_list = [];
function btn_add_class() {
	class_input_counter++;
	const newname = "class_input_" + class_input_counter;

	const r = document.getElementById('classes_boxes');
	p = document.createElement('input');
	p.setAttribute("id", newname)
	r.appendChild(p);
	const input = document.getElementById(newname);
	create_input_component(input, CLASSES);
	class_input_list.push(input);
	input.focus();
}

var tag_input_counter = 0;
const tags_input_list = [];
function btn_add_tag() {
	tag_input_counter++;
	const newname = "tag_input_" + tag_input_counter;

	const r = document.getElementById('tags_boxes');
	p = document.createElement('input');
	p.setAttribute("id", newname)
	r.appendChild(p);
	const input = document.getElementById(newname);
	create_input_component(input, TAGS);
	tags_input_list.push(input);
	input.focus();
}

var filetypes_input_counter = 0;
const filetypes_input_list = [];
function btn_add_filetype() {
	filetypes_input_counter++;
	const newname = "filetypes_input_" + filetypes_input_counter;

	const r = document.getElementById('filetypes_boxes');
	p = document.createElement('input');
	p.setAttribute("id", newname)
	r.appendChild(p);
	const input = document.getElementById(newname);
	create_input_component(input, FILETYPES);
	filetypes_input_list.push(input);
	input.focus();
}

var fnfragments_input_counter = 0;
const fnfragments_input_list = [];
function btn_add_fnfragment() {
	fnfragments_input_counter++;
	const newname = "fnfragments_input_" + fnfragments_input_counter;

	const r = document.getElementById('fnfragments_boxes');
	p = document.createElement('input');
	p.setAttribute("id", newname)
	r.appendChild(p);
	const input = document.getElementById(newname);
	create_input_component(input, FNFRAGMENTS);
	fnfragments_input_list.push(input);
	input.focus();
}

function render_filedata(filedata) {
	const d = (filedata.is_content && undefined != filedata.owner) ? filedata.owner.dirs.slice() : filedata.dirs.slice();
	if (filedata.is_content && undefined != filedata.owner) {
		d.push(filedata.owner.filename);
		d.concat(filedata.dirs);
	}
	const dir 	= d.join('/');
	const name 	= (undefined != filedata.alias && filedata.alias) ? filedata.alias : filedata.filename;

	const r = "<div style=\"position: relative;\">"
				+ "<a href=\"" + filedata.url + '"'
						+ (filedata.description ? " title = \"" + escapeHtml(filedata.description) + '"' : "")
						+ '">'
					+ "<img style=\"position: absolute; top: 50%; margin-top: -2ch; height: inherit; max-width: 4ch; width: auto;\" src=\""
						+ (	filedata.is_content ? "./img/icon.file.png"
							: filedata.is_cabinet ? "./img/icon.floppydisk.png"
							: "./img/icon.unknown.png"
						) + "\"/>"
				+ "</a>"
				+ "<span style=\"margin-left: 5ch; display: block;\">"
					+ dir + "&nbsp;:<br/>"
					+ "&nbsp;&nbsp;&nbsp;&nbsp;"
						+ "<a href=\"" + filedata.url + '"'
						+ (filedata.description ? " title = \"" + escapeHtml(filedata.description) + '"' : "")
						+ '">'
						+ name
						+ "</a>"
						+ (filedata.description ? "&nbsp;&#x2139;" : "")
				+ "</span>"
		+ "</div>"
		;
	return r;
}

function create_input_component(input, cache) {
	return 	new Awesomplete(input, {
		list: cache().map(function(cv, i, a) {return cv[0];}),
		autocomplete: "on",
		"aria-autocomplete": "list"
	});

}

function btn_doIt(renderer = render_filedata) {
	action_do_search(['fsid', 'url', 'file_type', 'is_cabinet', 'is_content', 'filename', 'alias', 'cabinet_format', 'dirs', 'description', 'owner'], renderer)
}
