require(["./js/jsonp.js", "./js/cache.js"], common_init);

function common_init() {
	JSONP.init({
		error: function(ex){
			const v = document.getElementById('version')
			while (v.firstChild) {
				v.removeChild(v.firstChild);
			}
			p = document.createElement('span');
			p.innerHTML = "err:" + ex.event.toString();
			v.appendChild(p);
		}
	});
	update_version();
};

function update_version() {
	JSONP.get(REQUEST('version'), {}, function(data) {
		const v = document.getElementById('version')
		while (v.firstChild) {
			v.removeChild(v.firstChild);
		}
		p = document.createElement('span');
		p.innerHTML = data.app + " " + data.code;
		v.appendChild(p);
		checkConsistency(data);
	});
}

function action_do_search(search_fields, renderer) {
	const parm_keywords = [];
	for (var i in keywords_input_list)
		parm_keywords.push(keywords_input_list[i].value);

	const parm_classes = [];
	for (var i in class_input_list)
		parm_classes.push(class_input_list[i].value);

	const parm_tags = [];
	for (var i in tags_input_list)
		parm_tags.push(tags_input_list[i].value);

	const parm_filetypes = [];
	for (var i in filetypes_input_list)
		parm_filetypes.push(filetypes_input_list[i].value);

	const parm_fnfragments = [];
	for (var i in fnfragments_input_list)
		parm_fnfragments.push(fnfragments_input_list[i].value);

	JSONP.get(REQUESTAPP('files/search'),
			{ keywords:parm_keywords, classes:parm_classes, tags:parm_tags, file_types:parm_filetypes, fn_fragments:parm_fnfragments, fields:search_fields },
			function(data) {
		const r = document.getElementById('result')
		while (r.lastChild) r.removeChild(r.lastChild);

		p = document.createElement('p');
		p.innerHTML = data.success_msg;
		r.appendChild(p);

		for (var i in data.files) {
			const f = data.files[i]
			p = document.createElement('p');
			p.innerHTML = renderer(f);
			r.appendChild(p);
		}
	});
}

function btn_clear_all() {
	{
		const r = document.getElementById('keywords_boxes');
		while (r.lastChild) r.removeChild(r.lastChild);
		keywords_input_list.length = 0;
	}
	{
		const r = document.getElementById('classes_boxes');
		while (r.lastChild) r.removeChild(r.lastChild);
		class_input_list.length = 0;
	}
	{
		const r = document.getElementById('tags_boxes');
		while (r.lastChild) r.removeChild(r.lastChild);
		tags_input_list.length = 0;
	}
	{
		const r = document.getElementById('filetypes_boxes');
		while (r.lastChild) r.removeChild(r.lastChild);
		filetypes_input_list.length = 0;
	}
	{
		const r = document.getElementById('fnfragments_boxes');
		while (r.lastChild) r.removeChild(r.lastChild);
		fnfragments_input_list.length = 0;
	}
	{
		const r = document.getElementById('result');
		while (r.lastChild) r.removeChild(r.lastChild);
	}
}

