require(["./js/jsonp.js", "./js/lz-string.js"], cache_init);

var cache_KEYWORDS;
var cache_CLASSES;
var cache_TAGS;
var cache_FILETYPES;
var cache_FNFRAGMENTS;

function cache_init() {
}

function checkConsistency(data) {
	if (localStorage.service_name && localStorage.service_name == data.app && localStorage.service_version && localStorage.service_version == data.ver) {
	} else {
		localStorage.clear();
		localStorage.service_name = data.app;
		localStorage.service_version = data.ver;
	}
}

function cacheClearItem(name) {
	if (localStorage.name)
		localStorage.removeItem(name);
}

function cacheClear() {
	cacheClearItem("cache_"+APP_ROOT+"_gettime");
	cacheClearItem("cache_"+APP_ROOT+"_CLASSES");
	cacheClearItem("cache_"+APP_ROOT+"_TAGS");
	cacheClearItem("cache_"+APP_ROOT+"_FILETYPES");
	cacheClearItem("cache_"+APP_ROOT+"_FNFRAGMENTS");
	cacheClearItem("cache_"+APP_ROOT+"_KEYWORDS");
}

function cacheCheck() {
	var nname = "cache_"+APP_ROOT+"_gettime";
	if ( localStorage.getItem(nname) && ((new Date().getTime() - localStorage.getItem(nname)) < 604800000) ) {
		return true;
	} else {
		cacheClear();
		return false;
	}
}

function cacheExists(name) {
	var nname = "cache_"+APP_ROOT+"_" + name;
	if ( localStorage.getItem(nname) ) {
		return true;
	} else {
		return false;
	}
}

function cacheSave(name, value) {
	var nname = "cache_"+APP_ROOT+"_"+name;
	localStorage.setItem(nname, LZString.compress(JSON.stringify(value)));
	var nname = "cache_"+APP_ROOT+"_gettime";
	localStorage.setItem(nname, new Date().getTime());
}

function cacheLoad(name) {
	var nname = "cache_"+APP_ROOT+"_"+name;
	return eval(LZString.decompress(localStorage.getItem(nname)));
}
