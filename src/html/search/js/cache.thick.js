require(['./js/jsonp.js', './js/util.js', './js/cache.js', './js/lz-string.js'], cache_thick_init);

function cache_thick_init() {
	cache_KEYWORDS = KEYWORDS();
	cache_CLASSES = CLASSES();
	cache_TAGS = TAGS();
	cache_FILETYPES = FILETYPES();
	cache_FNFRAGMENTS = FNFRAGMENTS();
}

function KEYWORDS() {
	if (cache_KEYWORDS && cacheCheck()) return cache_KEYWORDS;
	if (cacheExists("KEYWORDS")) return cache_KEYWORDS = cacheLoad("KEYWORDS");

	JSONP.get(REQUESTAPP('keywords/list'), {}, function(data) {
		cache_KEYWORDS = data.keywords;
		cacheSave("KEYWORDS", cache_KEYWORDS);
	});
	return cache_KEYWORDS;
}

function CLASSES() {
	if (cache_CLASSES && cacheCheck()) return cache_CLASSES;
	if (cacheExists("CLASSES")) return cache_CLASSES = cacheLoad("CLASSES");

	JSONP.get(REQUESTAPP('classes/list'), {}, function(data) {
		cache_CLASSES = data.classes;
		cacheSave("CLASSES", cache_CLASSES);
	});
	return cache_CLASSES;
}

function TAGS() {
	if (cache_TAGS && cacheCheck()) return cache_TAGS;
	if (cacheExists("TAGS")) return cache_TAGS = cacheLoad("TAGS");

	JSONP.get(REQUESTAPP('tags/list'), {}, function(data) {
		cache_TAGS = data.tags;
		cacheSave("TAGS", cache_TAGS);
	});
	return cache_TAGS;
}

function FILETYPES() {
	if (cache_FILETYPES && cacheCheck()) return cache_FILETYPES;
	if (cacheExists("FILETYPES")) return cache_FILETYPES = cacheLoad("FILETYPES");

	if (cache_FILETYPES && 0 != cache_FILETYPES.lenght) return cache_FILETYPES;
	JSONP.get(REQUESTAPP('file_types/list'), {}, function(data) {
		cache_FILETYPES = data.file_types;
		cacheSave("FILETYPES", cache_FILETYPES);
	});
	return cache_FILETYPES;
}

function FNFRAGMENTS() {
	if (cache_FNFRAGMENTS && cacheCheck()) return cache_FNFRAGMENTS;
	if (cacheExists("FNFRAGMENTS")) return cache_FNFRAGMENTS = cacheLoad("FNFRAGMENTS");

	JSONP.get(REQUESTAPP('fn_fragments/list'), {}, function(data) {
		cache_FNFRAGMENTS = data.fn_fragments;
		cacheSave("FNFRAGMENTS", cache_FNFRAGMENTS);
	});
	return cache_FNFRAGMENTS;
}

