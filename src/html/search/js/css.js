function init() {
	const css = document.createElement('link');
    css.rel = "stylesheet";
    css.type = "text/css";
    
    // This shit is to overcome a Chrome/Firefox bug while handling href on link tags
    // Safari does it right!
    css.href = 'file:' == window.location.protocol
    		? ( window.screen.width > window.screen.height ? "./css/styles.landscape.css" : "./css/styles.portraid.css" )
    		: ( window.screen.width > window.screen.height ? "/search/css/styles.landscape.css" : "/search/css/styles.portraid.css" )
    		;

    const list = document.getElementsByTagName('head')
    const last = list[list.length-1];
    last.parentNode.insertBefore(css, last);
};

init();