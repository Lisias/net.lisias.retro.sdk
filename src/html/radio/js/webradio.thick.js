require(["./js/webradio.js"], webradio_thick_init);

function webradio_thick_init() {
}

function render_playlist(entry) {
	const r =
		"<b>" + entry.pos + '</b>'
		+ (
			entry.author && entry.title
				? " " + entry.author + " : " + entry.title
			: entry.author || entry.title
				? " " + (entry.author ? entry.author : entry.title)
			: ""
		)
		+ "<br/>"
		;
	return r;
}
