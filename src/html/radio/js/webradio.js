require(["./js/jsonp.js"], webradio_init);

function webradio_init() {
	buttons_temporarily_disable_all(5);
	fetch_provider_from_proxy_because_cors_sucks_and_dont_allow_me_to_redirect_services('webradio', CHANNEL, bootstrap);
};

var MIDIA_BUTTONS_DISABLED = true;
function buttons_all_set_disable(value) {
	MIDIA_BUTTONS_DISABLED = value;
	const buttons = document.getElementsByTagName('button');
	for (var i = 0; i < buttons.length; i++) {
		const button = buttons[i];
		const type = button.getAttribute('type') || 'submit'; // Submit is the default
		if ('music_status' == button.parentElement.className || 'query_result' == button.parentElement.parentElement.className)
			button.disabled = value;
	}
}
function buttons_temporarily_disable_all(time_in_secs) {
	buttons_all_set_disable(true);
	setTimeout(
		function() {
			update_version();
			buttons_all_set_disable(false);
		}
		, 1000 * time_in_secs
	);
}

function ws_response_handler(xhr, ok_function) {
	switch(xhr.readyState) {
		case XMLHttpRequest.UNSENT :{} break;
		case XMLHttpRequest.OPENED :{} break;
		case XMLHttpRequest.HEADERS_RECEIVED :{} break;
		case XMLHttpRequest.LOADING :{} break;
		case XMLHttpRequest.DONE :{
			msg = JSON.parse(xhr.response);
			if (msg.error) {
				const v = document.getElementById('version')
				while (v.firstChild) {
					v.removeChild(v.firstChild);
				}
				p = document.createElement('span');
				p.innerHTML = '<span class="error_msg">' + msg.error_msg + '</span>';
				v.appendChild(p);
				buttons_temporarily_disable_all(45);
			} else if (ok_function) {
				ok_function(msg);
			}
		} break;

		default: {
			console.log("INVALID STATE FOR ws_response_handler");
		} break;
	}
}

function btn_update_playlist() {
	const xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { ws_response_handler(xmlHttp, update_playlist_handler); }
	xmlHttp.open( "GET", PLAYERCHANNELAPP('list'), true ); // false for synchronous request
	xmlHttp.send("");
}

function btn_previous_music() {
	const xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { ws_response_handler(xmlHttp, update_current_music_and_call_credits_handler); }
	xmlHttp.open( "PUT", PLAYERCHANNELAPP('prev'), true ); // false for synchronous request
	xmlHttp.send("");
}

function btn_next_music() {
	const xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { ws_response_handler(xmlHttp, update_current_music_and_call_credits_handler); }
	xmlHttp.open( "PUT", PLAYERCHANNELAPP('next'), true ); // false for synchronous request
	xmlHttp.send("");
}

function btn_delete_music() {
	const xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { ws_response_handler(xmlHttp, update_current_music_and_call_credits_handler); }
	xmlHttp.open( "DELETE", PLAYERCHANNELAPP('current'), true ); // false for synchronous request
	xmlHttp.send("");
}

function btn_play_music(code) {
	const xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { ws_response_handler(xmlHttp, update_current_music_and_call_credits_handler); }
	xmlHttp.open( "PUT", PLAYERCHANNELAPP('play'), true ); // false for synchronous request
	xmlHttp.send( JSON.stringify(FILEDATA[code]) );
}

function update_player_handler(metadata) {
	const v = document.getElementById('audio_player');
	metadata.data.stations.some(function(item,index) {
			if (STATION == item.symbol) {
				v.src = item.url;
				update_current_music();
				return true;
			}
			return false;
		});
}
function update_player() {
	JSONP.get(PLAYERAPP('metadata'), {}, update_player_handler);
}

function update_playlist_handler(data) {
	const r = document.getElementById('result')
	while (r.lastChild) r.removeChild(r.lastChild);

	p = document.createElement('p');
	p.innerHTML = data.success_msg;
	r.appendChild(p);

	const list = data.entries.sort(function(a, b) {
		return a.pos - b.pos;
	});
	for (var i in list) {
		p = document.createElement('p');
		p.innerHTML = render_playlist(list[i]);
		r.appendChild(p);
	}
}
function update_current_music_and_call_credits_handler(data) {
	update_current_music_handler(data);
	update_credits();
}
function update_current_music_handler(data) {
	const v = document.getElementById('music_content')
	v.textContent = data.success_msg;
}
function update_current_music() {
	JSONP.get(PLAYERAPP(CHANNEL+'/current'), {}, update_current_music_handler);
}

function timeSince(seconds) {
	interval = Math.floor(seconds / 31536000);

	if (interval > 1) return interval + "y";

	interval = Math.floor(seconds / 2592000);
	if (interval > 1) return interval + "m";

	interval = Math.floor(seconds / 86400);
	if (interval >= 1) return interval + "d";

	interval = Math.floor(seconds / 3600);
	if (interval >= 1) return interval + "h";

	interval = Math.floor(seconds / 60);
	if (interval > 1) return interval + "m ";

	return Math.floor(seconds) + "s";
}
function update_credits_handler(data) {
	data = data.acl;
	document.getElementById('current_credits').textContent = data.credits;
	if (0 == data.hit) {
		document.getElementById('last_hit').textContent = " muito tempo (ou nunca)";
	} else {
		document.getElementById('last_hit').textContent = timeSince(data.utc - data.hit) + " atrás";
	}
}
function update_credits() {
	JSONP.get(ACLAPP('balance'), {}, update_credits_handler);
}

var FILEDATA = {};
function btn_myDoIt() {
	FILEDATA = {};
	btn_doIt(render_filedata_with_player);
}

function bootstrap() {
	update_player(); update_credits();
}
