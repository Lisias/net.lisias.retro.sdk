net.lisias.retro.radio.WS
============================

Documentação do W/S de "Controle Remoto" do WebRadio da Confederação (bleh... que nome dou pra isso?)

Não botei autenticação nem controle - logo, sim, o RPi está bem suscetível à um DoS. Testes empíricos demonstraram que o meu upstream doméstico é uma limitação, mas também um *deterrance* por efeito colateral. Um upstream decente, e o W/S efetivamente sufocaria o resto da máquina trabalhando nas responses. Não, não tem cache (isso vai ficar por conta do NGINX, quando isso subir pro retro.lisias.net - aliás, o QoS vai ficar por conta dele também).

Tudo roda num [Raspberry Pi 2011.12 Model B](https://www.adafruit.com/products/998). A idéia original era montar serviços que rodassem em computadores de recursos mínimos, daqueles que se leva para os Encontros de Retrocomputação para apoio ao evento. Usei um RPi porque era o que tinha na mão, mas isso vai rodar com certeza (talvez até melhor) naqueles Thin Client de segunda mão que tão pipocando na Sta Efigênia (tem de 50 a 100 reais).

O Mpd ainda é considerado α (alpha). Nenhuma funcionalidade está formalmente estabelecida, as interfaces podem mudar.


## Serviço REST W/S ##

Implementado em [Python 3](https://docs.python.org/3/) .

Uma fuçada rápida no cliente HTML já diz tudo o que precisa saber para usar o serviço, mas segue uma descrição rápida:

Dado o endereço base, que no momento é http_//home.lisias.net_8089/ (trocar _ por :), o request sempre segue a forma

	/<algumacoisa>.<formatodesejado).

Não usar o `<formatodesejado>` implica numa response em `text/plain` (formato `.txt`, adequado para consultas manuais e debugging). Também reconhece .json e .jsonp (json com padding, para furar cross-domain scripting) e o format binário de serialização do Python, Pickle (protocolo 3).

Os métodos HTTP reconhecidos são `GET`, `PUT` e `DELETE`, e os eventuais parâmetros da Query seguem a [RFC-3986](https://www.rfc-editor.org/info/rfc3986) (ou pelo menos, é o que o [Bottle](http://bottlepy.org/docs/dev/) me disse).

Se faz um request que existe mas com dados inconsistentes, recebe um 200 com um Response explicando o erro. Se você faz um request que não existe, recebe um 404. Se você faz um request que existe mas com dados inválidos, leva um 500 na orelha e fica por isso mesmo.

Toda resposta retorna um campo booleano chamado `error`. Se `False`, `success_msg` tem uma mensagem de confirmação (normalmente um contador) e os demais campos são o payload da Response - dependente de Request. Se houve um erro, `error_msg` dá a razão por extenso, e `error_code` um identificador único no sistema do erro, para automatizar tratamento de erro.

Uma resposta de sucesso **pode** retornar um campo `timetaken_in_secs` onde o tempo gasto, sem segundos, no processamento do request é informado num valor *float*. Responses com coleções **podem** ter este dado em cada item da coleção. Este campo é opcional, e pode ou não estar presente - incluindo entre um Request e outro do mesmo server.

### Requests implementados: ###

#### `GET /version.<fmt>` ####
retorna a identificação da aplicação e a versão corrente. Bom para saber se o serviço tá de pé (ping). Retorna os formatos reconhecidos pelo W/S (bom para saber o que vc pode usar na hora, já que novos formatos virão).

#### `GET /metadata.<fmt>` ####
retorna os *archives* reconhecidos pelo W/S (asimov, wos, etc). Na medida que novos *archives* são adicionados, vai-se atualizando esta chamada.

#### `GET /<archive>/metadata.<fmt>` ####
retorna as entidades reconhecidas por este *archive*, bem como seus verbos de ação. Cada entidade vem numa tupla, com o singular e com o plural. Se o Request retorna *UMA* entidade, use o singular. Se o Request retorna uma *coleção*, use o plural. Os verbos também vêm em tuplas, com o seu nome e uma lista das entidades que reconhecem.

Entidades ***singleton*** (ver [Protocolo Confederação](./protocol.md#providerssingleton)) são listadas em seu próprio atributo. No nosso caso, todas elas. :)

Exemplo:

* Request: `TODO`

* Response: `TODO`

Estas duas chamadas serão importantes na Confederação, uma vez que cada W/S será capaz de explicar o que entende e como acessar, e o agregador poderá fazer o roteamento. Também é legal para fazer um cliente mais espertinho (ao contrário das minhas páginas de exemplo, que são burrinhas de dar dó).

No WebRadio, o conceito de "Entidade" foi levemente pervertido. Uma Entidade, neste serviço, é uma lista de "canais" alternativos para o mesmo audiostream, permitindo que o cliente possa se conectar ao stream mais próximo de seu terminal. Não há mais o requisito de ser apenas dois (singular e plural), até porque não faz sentido neste serviço.

Por convenção, o nome do canal é seguido por ":" seguido de um identificador de stream.

#### Verbos implementados ####

Uma vez escolhido o `<archive>`, a mecânica é sempre a mesma:

`/<archive>/<entitiy|entities>/<verb>.<fmt>?<parms>` (onde parms é [RFC-3986](https://www.rfc-editor.org/info/rfc3986) )

A maioria dos *requests* segue a norma `/webradio/<channel>/<verb>.<fmt>?<parms>` onde o channel é o canal desejado (neste momento, apenas o `AmigaMod`).

##### **PUT** `/webradio/<channel>/play.<fmt>` #####

Pega o Objeto FileData no corpo da mensagem, baixa o arquivo descrito do repositório, adiciona na playlist corrente no ponto atual e toca.

O formato FileData é essencialmente o retornado pelo `search` do `net.lisias.retro.file.search.WS`.

Retorna uma mensagem de sucesso, ou uma de erro.

##### **GET** `/webradio/<channel>/list.<fmt>` #####

Retorna a *playlist* corrente, onde cada entrada fornece a posição, o author (se conhecido) e o título da música (se existente).

A ordem das músicas na *playlist* é aleatória, você precisa ordenar pelo campo `pos` se deseja usá-la numa ordem específica.

##### **PUT** `/webradio/<channel>/next.<fmt>` #####

Avança a playlist para a próxima música. Volta para a primeira se a corrente for a última.

Retorna uma mensagem de sucesso, ou uma de erro.

##### **PUT** `/webradio/<channel>/prev.<fmt>` #####

Retrocede a playlist para a música anteriro. Pula para a última se a corrente for a primeira.

Retorna uma mensagem de sucesso, ou uma de erro.

##### **GET** `/webradio/<channel>/current.<fmt>` #####

Retorna na mensagem de sucesso o nome da música, posição na fila e (se disponível) nome do artista numa string pronta para visualização.

Retorna uma mensagem de sucesso, ou uma de erro.

##### **GET** `/webradio/credits/balance.<fmt>` #####

WiP.

#### CORS

**TODO**


#### Requests "Confederados" ####

Para gerenciamento do Pool de Proxies (a "Confederação"), o seguinte request foi implementado.

É o único **POST** em todo o Serviço.

##### `/announce.<fmt>` #####

É usado por um **proxy.WS** para anunciar que este serviço foi incluído na sua Unidade Confederada.

Não é necessário tomar nenhuma atitude, mas espera-se que o Provider inclua o endereço mencionado no **Referer** na sua lista de proxies em que está registrado - para sair desregistrando durante o shutdown do serviço.


*Archives*
-----------

### Online

Servidores de arquivo sendo usados como fonte de dados para. Em parênteses, o `fsid` (normalmente com o mesmo nome do *archive* respectivo, mas não é uma regra pétrea).

* **AmigaMod**
	* ftp://ftp.modland.com/pub/modules/ (modland)


Endereço atual
--------------

http://search.lisias.net/retro/modland.thick.html

ou

http://search.lisias.net/retro/aminet.thick.html

Ambas as páginas da Engine de Busca de FTP podem ser usadas para alimentar a playlist do Web Radio.
