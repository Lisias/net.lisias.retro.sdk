## Páginas HTML (Client Side) ##

Não tem tanta firula, é HTML5+CSS3+JS e **nenhum** framework.

A página faz um request para o W/S, recebe um [JSONP](http://stackoverflow.com/questions/2067472/what-is-jsonp-all-about#2067584), "parseia" e monta a View. A parte bacana é que não usei AJAX nem JQUERY, dependência mínima. Funciona apenas em browsers modernos por conta disso.

Autocomplete com [Awesomplete](https://leaverou.github.io/awesomplete/). Cache local dos metadados do servidor (tanto para poupá-lo como para agilizar o client-side que estiver usando conexão limitada, como 3G). Há margem para otimizações, não me preocupei muito em economizar memória (eu podia criar uma única lista de autocomplete e compartilhar entre os campos de texto...)

Uso do localStorage para cache dos metadados do Server. Cache vence por mudança de versão do serviço e por tempo.

Ficou pesada num celular.... =/ Por conta disto, foram implementadas duas versões das páginas:

* Thick - Para clientes "Thick" (parrudos :-) ) ou seja, Desktops, Tablets ou Celulares com alto poder de processamento e bastante memória.

* Thin - Para clientes "Thin" (limitados), como NetBooks, thin-clients corporativos, PDF Viewers ou celulares populares.

As páginas também são espertinhas, sabem detectar quando a tela é mais larga que alta e vice-versa (*landscape* ou *portraid*) e ajustam o *layout* em função.

Hackear as páginas é simples: aquilo roda até na sua máquina local sem alterações - salve tudo na sua máquina local e saia brincando. :-)


### *Interesting issues solved* ###

#### localStorage

O RPi tende à ficar sobrecarregado. Ainda mais, o objetivo é que estes serviços sejam prestados por pequenos dispositivos espalhados pelo mundo, sabe Deus em quais condições de largura de banda.

Adicionalmente, testes empíricos demonstram que os `list` das entidades são as chamadas que mais ferram a vida do serviço. E estar ocupando 85% da memória do dispositivo só com este serviço não tá ajudando muito: o *aftermath* é que as chamadas secundárias são simplesmente as mais lentas (algumas chegando a 30 segundos!), enquanto a *working horse* do sistema faz o trabalho dela em 1/10 de segundo. A escassez de memória dá a dica de que estamos sofrendo com as limitações do PyPy, uma vez que são exatamente estas chamadas as que fazem uso pesado de Strings (o ponto fraco do PyPy!), que gera muito lixo e obriga o GC do PyPy (uma carroça de bois mancos) à trabalhar constantemente.

Então as páginas de demonstração fizeram um uso bem agressivo de cache - no *startup* carregam todas as entidades (menos `files`) e armazenam num cache local (`localStorage`) durante uma semana ou até mudar a identificação do servidor (normalmente, versão).

Só que com a inclusão de novos *archives*, topei com uma situação inesperada: eu estourei a quota do localStorage!!

Pesquisas se revelaram infrutíferas - parece que ninguém sabe ao certo qual o tamanho da quota. Li sobre limites que variam de 10 megabytes até o espaço disponível em disco, ao passo que um código para pesquisar e solicitar mais espaço me informa que o limite corrente é de 2Giga. Mas este mesmo código me dizia que estava ocupando menos de 1K... Sei não. :-)

Tentei atacar a situação com bibliotecas de persistência, todas as opções se revelaram pior que o problema. Não me solucionavam o estouro que eu recebia ao tentar persistir o cache dos *archives* maiores, e ainda era mais uma coisa para aprender (e ainda ter que confiar no cara). Até que topei com alguém na StackOverflow comentando que havia um limite de 10 Megabytes *por item*. Hummm.... Agora começa a fazer sentido.

Passei a compactar (usado o [lz-string](https://github.com/pieroxy/lz-string/)) os dados antes de armazenar do localStorage, e o problema foi resolvido no curto prazo. Todo o localStorage agora está usando pouco mais de 2 megabytes (chequei no *filesystem*, uma vez que o maldito DevTools do Chrome tá travando quando tento analisar o localStorage por ele - belo pedaço de *****).

Pra os curiosos, no MacOS o Local Storage do Google está em `~/Library/Application Support/Google/Chrome/Default/Local Storage` .

#### Layout adaptável

Em tempo de carregamento, a página detecta se está numa tela *landscape* ou *portraid* (tablets e celulares), e adapta o layout de acordo.

Toda a mágica fica por conta do CSS - no fundo, há um CSS *vanilla* que é carregado junto com a página, e um script em tempo de execução detecta o ambiente e carrega um segundo CSS que complementa o primeiro - desta vez com o *layout* definitivo.

Mas nem tudo são flores, no entanto. Um CSS pode complementar um já carregado, mas desde que os elementos não colidam. Você não pode, por exemplo, mudar o tamanho de uma fonte do CSS anterior: então você cria o estilo sem o tamanho de fonte no *vanilla*, e depois adiciona esta tag em cada CSS específico.

E foi aí que os *browsers* modernos me morderam o calcanhar.

Uma vez carregado o CSS *vanilla*, a página é renderizada na primeira oportunidade e antes que a tag `script` com o código que escolhe o CSS complementar tenha chance de rodar. Isso faz o layout aparecer bagunçado num primeiro momento, quando então finalmente o código JS que atualiza o CSS roda - e então tudo é reajustado na cara do usuário. Péssimo.

Ainda não tenho uma solução para isso.

#### Thin Clients / Thick Clients

Os clientes HTML, na sua primeira encarnação, ficaram bem pesadas. No esforço de diminuir a carga dos *Providers*, usou-se agressivamente o `localStorage` e estruturas de dados em memória. Computadors clássicos, tablets datados, celulares modestos e similares rodam com dificuldade (quando rodam).

A necessidade de se criar versões leves destas páginas é, então, evidente.

O primeiro passo é diferenciar um *Thick Client* de um *Thin Client*. Isso é feito pelo `index.html` do grupo de páginas (hoje, `db` e `search`), que então redireciona para o `index.thick.html` ou para o `index.thin.html` e é isso.

A detecção é curta e grossa: análise do `User-Agent`.

#### CORS

**TODO**
