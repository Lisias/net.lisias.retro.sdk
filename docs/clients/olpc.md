net.lisias.retro.search.WS client no OLPC XO-1
==============================================

# Configuração e Instalação

Este documento explica como instalar o ambiente de execução descrito [aqui](console.md), mas para o OLPC XO-1.

O XO-1 ainda usa o Fedora 18, bem defasado, de forma que muitos mais passos são necessários que para uma máquina mais moderna.


## Sistema Operacional

Atualizar para o OLPC 13.2.8 (ou pelo menos 13.2.7) ( [instruções](http://wiki.laptop.org/go/Updating_the_XO) ) e então:

```bash
sudo yum -y update
```

Por algum motivo, o OLPC não inclui o diretório `/usr/local/bin` no path de sistema (embora coloque `/usr/local/sbin` ... ) Então teremos que fazer isso nós mesmos. Abra um terminal e digite o que segue:

```bash
echo 'if [[ $PATH != *"/usr/local/bin"* ]] ; then export PATH=/usr/local/bin:$PATH ; fi' | sudo tee /etc/profile.d/zzzz_usrlocalbin.sh
export PATH=/usr/local/bin:$PATH
sudo echo bash -c "echo ""'/usr/local/lib' > /etc/ld.so.conf/usrlocallib.conf"
```

Estes comandos vão configurar o sistema para incluir o `/usr/local/bin` no fim do $PATH durante o boot, e também adicionar na sessão corrente para que você não precise rebootar o aparelho agora. Colocar este diretório no fim do path causará alguns transtornos, como ter que usar o *pathname* completo na hora de usar alguns utilitários - mas foi necessário porque nem todas as versões atuais destes funcionarão corretamente numa distribuição já velhinha como o Fedora 18 e deixá-los usáveis pelo S.O. pode ser problemático. Melhor pecar pelo excesso de prudência que pela falta dela.

Também coloca o `/usr/local/lib` no PATH de bibliotecas dinâmicas, o que permitirá adicionar *features* extras sem mexer nas bibliotecas padrão do Fedora 18.

Isso feito, vamos baixar, compilar e instalar o Python 3.4. Digite os seguintes comandos no terminal:

```bash
mkdir source
cd source
wget https://www.python.org/ftp/python/3.4.6/Python-3.4.6.tgz
tar xzf Python-3.4.6.tgz
sudo yum -y install gzip gzip-devel bzip2 bzip2-devel zlib zlib-devel xz xz-devel xz-lzma-compat lzma-devel \
	ncurses ncurses-devel readline readline-devel \
	sqlite sqlite-devel openssl openssl-devel tcl tcl-devel tk tk-devel tkinter \
	make gcc gdbm gdbm-devel \
	python3 python-virtualenv ; \
cd Python-3.4.6 ; ./configure ; make && make test
```

E vá assistir algo enquanto o troço instala as dependências e depois compila e testa o binário gerado. A Trilogia Extendida do Senhor dos Anéis deve ser suficiente. :-)

Quando a compilação terminar e os testes forem concluídos (com sucesso), digite:

```bash
sudo make altinstall
python3.4
```

Isto vai demorar algum tempo também, possivelmente você quererá assistir a Trilogia do Hobbit desta vez. :-)

E então, finalmente, você deve ver no seu console algo parecido com:

```
Python 3.4.6 (default, Feb  2 2017, 05:02:22)
[GCC 4.7.2 20121109 (Red Hat 4.7.2-8)] on linux
Type "help", "copyright", "credits" or "license" for more information.
```

## Biblitecas de sistema

Por *default*, o Fedora 18 (base do OLPC XO-1) não vem com várias bibliotecas necessárias. Algumas não estão nem mesmo disponíveis em pacotes (como o Python 3.4 acima). Esta seção enumera os passos necessários para satisfazer as dependências.

### Instalando pacotes do Fedora 18

#### Comum à todos os projetos

```bash
sudo yum install gcc gcc-c++ make automake autoconf libtool yasm nasm git subversion
sudo yum install libarchive libarchive-devel
```

#### Necessário apenas para o *client-side*

Todos os comandos acima e abaixo precisam ser executados sem falha, senão o setup vai falhar. Isso inclui comandos aparentemente inócuos como `mkdir docs` ali embaixo.

### Instalando bibliotecas a partir dos Fontes

Por questões políticas (ou de idade mesmo) nem todos as bibliotecas e utilitários necessários possuem pacotes para o Fedora 18. Teremos que compilar tudo na unha.

Bom, vamos lá. Esse processo é estupidamente demorado, prepare as pipocas e a maratona de House MD. ;-)

#### LAME

Necessário apenas para alguns exemplos.

```bash
cd ~
mkdir source ; cd source
wget https://sourceforge.net/projects/lame/files/lame/3.99/lame-3.99.5.tar.gz/download -O lame-3.99.5.tar.gz
tar -xzf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure
make
sudo make install
sudo ldconfig
```

## Configurando um Ambiente de Execução

### Python

No OLPC alguns passos extras são necessários devido à idade avançada =P do S.O. base (Fedora 18).

Não são necessários privilégios especiais para executar os exemplos, e nem mesmo para levantar os Micro Services caso você opte por colaborar com a Confederação. O ambiente de execução é o mesmo tanto para os clientes fornecidos pelo SDK como para os W/S em si:

```bash
cd ~
mkdir net.lisias.retro
cd net.lisias.retro
git clone https://www.bitbucket.org/Lisias/net.lisias.retro.pub pub
git clone https://www.bitbucket.org/Lisias/net.lisias.retro.sdk sdk
```
O `virtualenv` do Fedora 18 é muito antigo, e não dá o menor suporte para o Python3. Não há atualização oficial (ao menos ainda). Para instalar o `virtualenv`, vamos precisar instalar primeiro o `pip`. Que também não tem versão atual o suficiente no Fedora 18.

Então vamos ter que compilar e instalar também estes dois carinhas.

```bash
cd ~
cd source
wget https://bootstrap.pypa.io/get-pip.py
sudo /usr/local/bin/python3.4 get-pip.py
sudo /usr/local/bin/python3.4 -m pip install virtualenv
```

Note que o `virtualenv` atualizado vai ser instalado em `/usr/local/bin`, mas para evitar que as ferramentas Pyhton 3.4 zoem com o [Sugar](http://wiki.laptop.org/go/Sugar), o `PATH` sistema foi configurado para colocar os binários do Python *default* (em `/usr/bin`) do Fedora 18 na frente. Quando a ferramenta tiver o mesmo nome (como o `virtualenv`), será sempre necessário usar o *pathname* completo. É um saco, mas evita crashar o OLPC, que foi construído em cima do Python 2.

E então, **finalmente** :-) :

```bash
cd ~
cd net.lisias.retro
/usr/local/bin/virtualenv --python python3.4 runtime
source runtime/bin/activate
easy_install pub/net.lisias.retro.WS-1.0.5b0-py3.4.egg
```

Isto também vai demorar um bom tempo. Como a Trilogia do Silmarillion ainda não foi filmada, não tenho uma boa sugestão para passar o tempo agora. :-D

O nome do arquivo `egg` vai variar com o tempo, na medida que novas versões forem publicadas.

#### Instalando dependências externas para o Python

##### misc

```bash
mkdir net.lisias.retro
cd ~/net.lisias.retro
source runtime/bin/activate
pip install libarchive-c
```

**IMPORTANTE**: é "libarchive-c", não apenas 'libarchive'. Este último é outra coisa, instala junto um monte de coisa que não usaremos e nem mesmo faz o que precisamos. Um saco pra limpar depois.

##### pyaudio

Necessário apenas para alguns exemplos.

```bash
source ~/net.lisias.retro/runtime/bin/activate
sudo yum install portaudio portaudio-devel
cd ~/source
git clone http://people.csail.mit.edu/hubert/git/pyaudio.git pyaudio
cd pyaudio
cat setup.py | sed "s/long_description=__doc__/long_description='duh'/g" > setup_hack.py
mkdir docs
python setup_hack.py install
```
Não me pergunte o motivo de existir do *hack*. Mas foi a forma que eu achei de fazer o treco funcionar.

### X11-Basic

Sim, clientes em BASIC. WiP mas vai ter. ;-)

É necessário ter o excelente [X11-Basic](http://x11-basic.sourceforge.net/) instalado. Como nada no OLPC é simples quando fora da caixinha, vamos à receitinha de bolo da vez:

```bash
cd source
git clone https://gitlab.com/kollo/X11Basic.git
cd X11Basic/src
./configure --prefix=/usr/local 
sudo mkdir -p /usr/local/share/applications
sudo mkdir -p /usr/local/share/icons/hicolor/48x48/apps/
make 
sudo make install
sudo ldconfig
```

## Testes e uso

Ver [console](./console.md#testeseuso).
