net.lisias.retro.search.WS client
=================================

## Requisitos Mínimos

Os clientes descritos aqui possuem os seguintes requisitos:

* Todo e qualquer sistema operacional/máquina com:
	* Python 3.4
	* virtualenv

Contudo, se você deseja rodar um servidor, você precisará que sua *box* também dê suporte para:

* gevent
* greenlet
* netifaces

O que no momento limita um pouco as opções.

É garantido que os Micro Services rodem em:

* Mac OSX
* Raspberry PI sob Raspian (Debian Jessie)
* Xeon Irwindale (Dell 1425SC) sob Devuan Jessie
* AWS EC2 sob Gentoo Base 10.2 e 10.3
* [OLPC](./olpc.md) 13.2.7 e 13.2.8 (instruções específicas no link)
	* Algumas dependências só compilaram no 13.2.8 (não pergunte a razão, apenas percebi que muitos problemas de compilação sumiram quando atualizei pro 2.8)

Mas estes são apenas as *boxes* em que eu levantei os serviços com sucesso. Teoricamente deve rodar em qualquer máquina desde que debaixo de um Linux, BSD ou Windows 7 pra cima. O ponto de corte é o suporte para as dependências que listei acima.

Suporte para versões mais recentes de Python (3.5 e 3.6) são RiP no momento. Bons resultados foram obtidos, mas optei por manter a 3.4 como recomendada até o Raspbian atualizar o Python dele.

Você não está amarrado ao Python 3.4 (ou nem mesmo ao Python) para criar seus clientes (ver [clientes html](./html.md)). Se a sua linguagem de escolha puder fazer HTTP REST, então você pode usá-la. Haverão algumas dificuldades, no entanto, para construir Micro Services em outra plataforma que não a descrita neste documento, pois não há planos no curto prazo de portar a biblioteca de suporte. Mas você pode criar a sua.


## Configurando um Ambiente de Execução

Não são necessários privilégios especiais para executar os exemplos, e nem mesmo para levantar os Micro Services caso você opte por colaborar com a Confederação. O ambiente de execução é o mesmo tanto para os clientes fornecidos pelo SDK como para os W/S em si:

```bash
cd ~
mkdir net.lisias.retro
cd net.lisias.retro
git clone https://www.bitbucket.org/Lisias/net.lisias.retro.pub pub
git clone https://www.bitbucket.org/Lisias/net.lisias.retro.sdk sdk
virtualenv --python python3.4 runtime
source runtime/bin/activate
easy_install pub/net.lisias.retro.WS-1.0.4-py3.4.egg
```

O nome do arquivo egg vai variar com o tempo, na medida que novas versões forem publicadas.


## Testes e uso

Finalizando a instalação das dependências e da biblioteca da Confederação, vamos conferir se estamos corretamente acessando a Confederação:

```bash
[olpc@olpc net.lisias.retro]$ wget -q -O - http://home.lisias.net:8090/report

net.lisias.retro Confederation of Retrocomputing Micro Services.

REQUEST: GET http://home.lisias.net:8090/report

archives :
	funet.msx :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	gaby :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	aminet :
		providers : {'http://home.lisias.net:8082/'}
		count : 1
	simtel :
		providers : {'http://home.lisias.net:8082/'}
		count : 1
	funet.amiga :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	funet.atari :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	whtech :
		providers : {'http://home.lisias.net:8082/'}
		count : 1
	hobbes :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	wos :
		providers : {'http://home.lisias.net:8083/'}
		count : 1
	garbo :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	x2ftp :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	tv-dog :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	freedos :
		providers : {'http://home.lisias.net:8083/'}
		count : 1
	modland :
		providers : {'http://home.lisias.net:8089/', 'http://home.lisias.net:8082/'}
		count : 2
	asimov :
		providers : {'http://home.lisias.net:8083/'}
		count : 1
	hornet :
		providers : {'http://home.lisias.net:8081/'}
		count : 1
	arnold :
		providers : {'http://home.lisias.net:8083/'}
		count : 1


To get help : http://service.retro.lisias.net/
```

A resposta pode variar, em especial a ordem dos *Archives*.


### examples.console Package


#### super_search

Este exemplo demonstra como fazer uma busca em cada um dos *archives* disponíveis e consolidar os resultados.

```bash
source ~/net.lisias.retro/runtime/bin/activate
cd ~/net.lisias.retro/sdk/src/python
python -m examples.console.super_search
```

Se tudo ocorreu como esperado, você terá o seguinte resultado:

```
net.lisias.retro.proxy.WS 0.2.1 α (0.3.2 β)
LostInLove.lha
	ftp://main.aminet.net/mods/slc/LostInLove.lha
AK_Hands.lha
	ftp://main.aminet.net/mods/med/AK_Hands.lha
Micro Tennis (1983)(DaTaBioTics)(Part 1 of 2)[AKA Love Tennis].zip
	ftp://ftp.whtech.com/emulators/mess/old/Complete MESS Geneve emulation/mess/software/ti99_4a/Micro Tennis (1983)(DaTaBioTics)(Part 1 of 2)[AKA Love Tennis].zip
influv2f.zip
	ftp://hornet.scene.org/pub/demosdemos/1998/i/influv2f.zip
DeniedLove.lha
	ftp://main.aminet.net/mods/emax/DeniedLove.lha
1love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Strobo/1love.mod
StrangeLove.trd.zip
	https://wos.meulie.net/pub/sinclair/trdos/misc/StrangeLove.trd.zip
LoveStinks.lha
	ftp://main.aminet.net/mods/funet/LoveStinks.lha
BelieveInLove.lha
	ftp://main.aminet.net/mods/synth/BelieveInLove.lha
ifu-love.lha
	ftp://main.aminet.net/mods/sidew/ifu-love.lha
cpu-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1997/c/cpu-love.zip
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Motorroller/love.mod
LoveQuiz2.z80.zip
	https://wos.meulie.net/pub/sinclair/games/l/LoveQuiz2.z80.zip
NothingForLove.lha
	ftp://main.aminet.net/mods/misc/NothingForLove.lha
Can't help falling in love.mp3
	ftp://ftp.whtech.com/music/mp3 files/Can't help falling in love.mp3
artilove.lha
	ftp://main.aminet.net/mods/uns/artilove.lha
faraway.lha
	ftp://main.aminet.net/mods/wmr/faraway.lha
PinkLove.mpg
	ftp://main.aminet.net/mods/elbie/PinkLove.mpg
slow'n'love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Pye/slow'n'love.mod
LoveMeBaby.scr
	https://wos.meulie.net/pub/sinclair/screens/load/l/scr/LoveMeBaby.scr
Love is blue.mp3
	ftp://ftp.whtech.com/music/mp3 files/Love is blue.mp3
love.psid
	ftp://ftp.modland.com/pub/modules/PlaySID/Vip/love.psid
love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1998/l/love.zip
Fallenherp.lha
	ftp://main.aminet.net/mods/misc/Fallenherp.lha
djf-urlove.lzh
	ftp://main.aminet.net/mods/med/djf-urlove.lzh
MUIWB-pic.lha
	ftp://main.aminet.net/pix/wb/MUIWB-pic.lha
love-serenade.mod
	ftp://ftp.modland.com/pub/modules/Fasttracker/Sly/love-serenade.mod
me_and_my_love.lha
	ftp://main.aminet.net/mods/techn/me_and_my_love.lha
Looking for love.mp3
	ftp://ftp.whtech.com/music/mp3 files/Looking for love.mp3
R14-I_Love.lha
	ftp://main.aminet.net/mods/sets/R14-I_Love.lha
Pussy-LoveStoryFromTitanic.gif
	https://wos.meulie.net/pub/sinclair/screens/load/p/gif/Pussy-LoveStoryFromTitanic.gif
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Drac/love.mod
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Emo/love.mod
love.jpg
	ftp://main.aminet.net/pix/trace/love.jpg
lns67_belEp2.lha
	ftp://main.aminet.net/mods/techn/lns67_belEp2.lha
love-s.ba
	ftp://ftp.whtech.com/club100/mus/love-s.ba
cgs-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1996/c/cgs-love.zip
Firth_love.lha
	ftp://main.aminet.net/mods/slow/Firth_love.lha
love+police=peace!.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Broom/love+police=peace!.mod
Pussy-LoveStoryFromTitanic.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/p/Pussy-LoveStoryFromTitanic.gif
StorieOfLove.lha
	ftp://main.aminet.net/mods/chryl/StorieOfLove.lha
phd_love.lha
	ftp://main.aminet.net/mods/airon/phd_love.lha
kaosasal.lha
	ftp://main.aminet.net/mods/jorma/kaosasal.lha
love.cmd
	ftp://ftp.gaby.de1.biz/2/268517_81539/pub/cpm/znode51/pcwworld/d103_1/user_1/love.cmd
gfd2love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1996/g/gfd2love.zip
TangoLoveSong.lha
	ftp://main.aminet.net/mods/rated/TangoLoveSong.lha
LoveJoy's Preparation for the GMAT - Exam Two.dsk
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/LoveJoy's Preparation for the GMAT - Exam Two.dsk
love-energy.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Extacy/love-energy.mod
BinaryLove.tap.zip
	https://wos.meulie.net/pub/sinclair/demos/b/BinaryLove.tap.zip
anotlove.lha
	ftp://main.aminet.net/mods/jorma/anotlove.lha
mvp_0042.lha
	ftp://main.aminet.net/mods/mvp/mvp_0042.lha
loveinvein.lha
	ftp://main.aminet.net/mods/misc/loveinvein.lha
yago-celebrate-the-love-bero-remix.pt2
	ftp://ftp.modland.com/pub/modules/Picatune2/BeRo/yago-celebrate-the-love-bero-remix.pt2
we_love.lha
	ftp://main.aminet.net/mods/med/we_love.lha
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Vegard/love.mod
livelove.zip
	ftp://hornet.scene.org/pub/demosdemos/1995/l/livelove.zip
SecretsOfLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/s/SecretsOfLove.gif
i_love_you.jpg
	ftp://main.aminet.net/pix/trace/i_love_you.jpg
love-song.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Daxx/love-song.mod
Pussy-LoveStoryFromTitanic.png
	https://wos.meulie.net/pub/sinclair/games-maps/p/Pussy-LoveStoryFromTitanic.png
Agn-RealLove.lha
	ftp://main.aminet.net/mods/techn/Agn-RealLove.lha
stonelov.lha
	ftp://main.aminet.net/mods/casef/stonelov.lha
kan-groo-love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Chriz/kan-groo-love.mod
n_plp.zip
	ftp://hornet.scene.org/pub/demosdemos/1997/n/n_plp.zip
CrazyLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/c/CrazyLove.gif
LoveOfMyLife.lha
	ftp://main.aminet.net/mods/melod/LoveOfMyLife.lha
a_palove.lha
	ftp://main.aminet.net/mods/misc/a_palove.lha
what-is-love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Hellrazor/what-is-love.mod
tst_lpmm.lha
	ftp://main.aminet.net/mods/med/tst_lpmm.lha
jjj_love.lha
	ftp://main.aminet.net/mods/s3m/jjj_love.lha
love3.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Josyf/love3.mod
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Psimon/love.mod
Pussy-LoveStoryFromTitanicDemo.trd.zip
	https://wos.meulie.net/pub/sinclair/trdos/games/p/Pussy-LoveStoryFromTitanicDemo.trd.zip
SummerLove.lha
	ftp://main.aminet.net/mods/synth/SummerLove.lha
ftj_love.zip
	ftp://hornet.scene.org/pub/demosdemos/1995/f/ftj_love.zip
m_love.lha
	ftp://main.aminet.net/mods/misc/m_love.lha
LoveJoy's Preparation for the GMAT - Practice Topics 1-11.dsk
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/LoveJoy's Preparation for the GMAT - Practice Topics 1-11.dsk
McInLove.lha
	ftp://main.aminet.net/mods/piano/McInLove.lha
Love_Synth.lha
	ftp://main.aminet.net/mods/melod/Love_Synth.lha
love-sale.psid
	ftp://ftp.modland.com/pub/modules/PlaySID/Harlequin/love-sale.psid
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Griff/love.mod
lovespy.zip
	ftp://ftp.funet.fi/pub/atari/sound/lovespy.zip
CrazyLove.tap.zip
	https://wos.meulie.net/pub/sinclair/demos/c/CrazyLove.tap.zip
FirstLove.lha
	ftp://main.aminet.net/mods/slow/FirstLove.lha
pb_love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1996/p/pb_love.zip
Romper Room's I Love My Alphabet.nib
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/Romper Room's I Love My Alphabet.nib
LoveJoy's Preparation for the GMAT - Practice Topics 21-25.dsk
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/LoveJoy's Preparation for the GMAT - Practice Topics 21-25.dsk
love.it
	ftp://ftp.modland.com/pub/modules/Impulsetracker/Neozombie/love.it
nin_love.lha
	ftp://main.aminet.net/mods/s3m/nin_love.lha
Love.lha
	ftp://main.aminet.net/mods/xm/Love.lha
what_is_love.lha
	ftp://main.aminet.net/mods/pro/what_is_love.lha
Pussy-LoveStoryFromTitanic.tap.zip
	https://wos.meulie.net/pub/sinclair/games/p/Pussy-LoveStoryFromTitanic.tap.zip
LoveBoat.lha
	ftp://main.aminet.net/mods/pop/LoveBoat.lha
LoveJoy's Preparation for the GMAT - Exam One.dsk
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/LoveJoy's Preparation for the GMAT - Exam One.dsk
love.psid
	ftp://ftp.modland.com/pub/modules/PlaySID/Fanta/love.psid
FirtreeLove.lha
	ftp://main.aminet.net/mods/melod/FirtreeLove.lha
love.bas
	ftp://ftp.gaby.de1.biz/2/268517_81539/pub/cpm/znode51/pcwworld/ga103/user_0/love.bas
love&devotion.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Opal/love&devotion.mod
RomperRoomsILoveMyAlphabet.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/r/RomperRoomsILoveMyAlphabet.gif
AReasonToLove.lha
	ftp://main.aminet.net/mods/med/AReasonToLove.lha
er_summe.lha
	ftp://main.aminet.net/mods/techn/er_summe.lha
amu-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1996/a/amu-love.zip
s7-gener.lha
	ftp://main.aminet.net/mods/sonor/s7-gener.lha
love.it
	ftp://ftp.modland.com/pub/modules/Impulsetracker/Phreak/love.it
love.mod
	ftp://ftp.modland.com/pub/modules/Fasttracker/Shad/love.mod
AKindOfLove.lha
	ftp://main.aminet.net/mods/chrom/AKindOfLove.lha
TrueLove.gif
	https://wos.meulie.net/pub/sinclair/screens/load/t/gif/TrueLove.gif
emod.love.lha
	ftp://main.aminet.net/mods/pro/emod.love.lha
aw-gnl.lha
	ftp://main.aminet.net/mods/pro/aw-gnl.lha
SkiesOfLove.lha
	ftp://main.aminet.net/docs/hyper/SkiesOfLove.lha
one-love-maxi.mod
	ftp://ftp.modland.com/pub/modules/Protracker/T-Bozz/one-love-maxi.mod
ctrl-alt-love.psid
	ftp://ftp.modland.com/pub/modules/PlaySID/Ferrara/ctrl-alt-love.psid
lem_love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1998/l/lem_love.zip
UP-TRNON.lha
	ftp://main.aminet.net/mods/uprgh/UP-TRNON.lha
fcy-love.zip
	ftp://hornet.scene.org/pub/demosgraphics/images/1998/f/fcy-love.zip
badboy.lzh
	ftp://main.aminet.net/mods/panik/badboy.lzh
With love in my heart.mp3
	ftp://ftp.whtech.com/music/mp3 files/With love in my heart.mp3
HotelLove.tap.zip
	https://wos.meulie.net/pub/sinclair/demos/h/HotelLove.tap.zip
k_give2.lha
	ftp://main.aminet.net/mods/wmr/k_give2.lha
AFoolInLove.lha
	ftp://main.aminet.net/mods/delor/AFoolInLove.lha
c64love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Anak/c64love.mod
LoveParade.lha
	ftp://main.aminet.net/pix/illu/LoveParade.lha
LoveJoy's Preparation for the GMAT - Practice Topics 12-20.dsk
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/LoveJoy's Preparation for the GMAT - Practice Topics 12-20.dsk
psy_love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1997/p/psy_love.zip
TrueLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/t/TrueLove.gif
ifu-love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Sidewinder/ifu-love.mod
Etr_YrLoveSavM.lha
	ftp://main.aminet.net/mods/pro/Etr_YrLoveSavM.lha
sn3-wld2.lha
	ftp://main.aminet.net/demo/aga/sn3-wld2.lha
love_anarchy.lha
	ftp://main.aminet.net/mods/demo/love_anarchy.lha
taste.lha
	ftp://main.aminet.net/mods/ooze/taste.lha
1love.lha
	ftp://main.aminet.net/mods/techn/1love.lha
first-love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Sleipner/first-love.mod
And I love her.mp3
	ftp://ftp.whtech.com/music/mp3 files/And I love her.mp3
Yazz-StandUpForYourLoveRights.z80.zip
	https://wos.meulie.net/pub/sinclair/demos/y/Yazz-StandUpForYourLoveRights.z80.zip
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Lucifer/love.mod
otb_love.zip
	ftp://hornet.scene.org/pub/demosdemos/1997/o/otb_love.zip
LoveLight.jpg
	ftp://main.aminet.net/pix/nevil/LoveLight.jpg
Love.lha
	ftp://main.aminet.net/mods/misc/Love.lha
love.exe
	ftp://ftp.sunet.se/mirror/archive/ftp.sunet.se/pub/simtelnet/win95/scrsave/love.exe
LoveBusinessAndTheZodiac.jpg
	https://wos.meulie.net/pub/sinclair/zx81/games-inlays/LoveBusinessAndTheZodiac.jpg
irs_love.lha
	ftp://main.aminet.net/demo/ecs/irs_love.lha
1sight.lha
	ftp://main.aminet.net/mods/slc/1sight.lha
Fireground-LoveMachine-SilentNight-BoomRAngle.DSK
	ftp://ftp.apple.asimov.net/pub/apple_II/images/games/file_based/Fireground-LoveMachine-SilentNight-BoomRAngle.DSK
love.mod
	ftp://ftp.modland.com/pub/modules/Protracker/Xhale/love.mod
jjj-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1995/j/jjj-love.zip
vsn_llb.lha
	ftp://main.aminet.net/demo/40k/vsn_llb.lha
But know I love you.mp3
	ftp://ftp.whtech.com/music/mp3 files/But know I love you.mp3
StrangeLove.scr
	https://wos.meulie.net/pub/sinclair/screens/load/s/scr/StrangeLove.scr
LoveOpensYourE.lha
	ftp://main.aminet.net/mods/instr/LoveOpensYourE.lha
LoveTakes.lha
	ftp://ftp.funet.fi/pub/amiga/demos/PARTIES/Gathering94/graphics/LoveTakes.lha
elemlove.lha
	ftp://main.aminet.net/mods/pro/elemlove.lha
pn_LoveAndDeat.lha
	ftp://main.aminet.net/mods/melod/pn_LoveAndDeat.lha
love.psid
	ftp://ftp.modland.com/pub/modules/PlaySID/Aegis/love.psid
m6r-love.zip
	ftp://hornet.scene.org/pub/demosmusic/contests/mc6/rookie/m6r-love.zip
rez_WeAllLoveR.lha
	ftp://main.aminet.net/mods/tranc/rez_WeAllLoveR.lha
lovemem.mp3
	ftp://main.aminet.net/mods/kicko/lovemem.mp3
mod.LessonsInLove.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1992/mod.LessonsInLove.lha
LadyInLoveDemo.trd.zip
	https://wos.meulie.net/pub/sinclair/trdos/games/l/LadyInLoveDemo.trd.zip
LoveStory.lha
	ftp://main.aminet.net/mods/funet/LoveStory.lha
up-ilove.lha
	ftp://main.aminet.net/mods/uprgh/up-ilove.lha
LoveHell.lha
	ftp://main.aminet.net/mods/voice/LoveHell.lha
mod.LoveStory.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1994/mod.LoveStory.lha
mod.Love-Main_Theme.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/JogeirLiljedahl/mod.Love-Main_Theme.lha
lostlove.zip
	ftp://hornet.scene.org/pub/demosdemos/1997/l/lostlove.zip
BinaryLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/b/BinaryLove.gif
LoveFunk.lha
	ftp://main.aminet.net/mods/misc/LoveFunk.lha
Fairlight.LOVE.lha
	ftp://ftp.funet.fi/pub/amiga/demos/Fairlight/Fairlight.LOVE.lha
lovecalc.lha
	ftp://main.aminet.net/misc/misc/lovecalc.lha
mod.love_you_right.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/ginseng-alchemy/mod.love_you_right.lha
ChangeMyLove.lha
	ftp://main.aminet.net/mods/chryl/ChangeMyLove.lha
mod.sea_of_love.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/frederic_motte/mod.sea_of_love.lha
mod.definition_of_love.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/mod.definition_of_love.lha
lovedemo.zip
	ftp://hornet.scene.org/pub/demosdemos/1992/lovedemo.zip
HotelLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/h/HotelLove.gif
EnergyofLove.lha
	ftp://main.aminet.net/mods/misc/EnergyofLove.lha
up-tbilr.lha
	ftp://main.aminet.net/mods/techn/up-tbilr.lha
LoveDungeon_1.0_disk.adf
	ftp://main.aminet.net/game/actio/LoveDungeon_1.0_disk.adf
mod.LoveYouRight.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.LoveYouRight.lha
tfa_love.zip
	ftp://hornet.scene.org/pub/demosdemos/1995/t/tfa_love.zip
Yazz-StandUpForYourLoveRights.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/y/Yazz-StandUpForYourLoveRights.gif
LoveBass.lha
	ftp://main.aminet.net/mods/misc/LoveBass.lha
LoveMice.lha
	ftp://ftp.funet.fi/pub/amiga/fish/601-700/ff660/LoveMice.lha
DontNeedYourLove.lha
	ftp://ftp.funet.fi/pub/amiga/demos/PARTIES/Party93/music/exe/DontNeedYourLove.lha
LoveMeBaby.gif
	https://wos.meulie.net/pub/sinclair/screens/load/l/gif/LoveMeBaby.gif
haujobb.lha
	ftp://main.aminet.net/demo/40k/haujobb.lha
sy_loft.lha
	ftp://main.aminet.net/mods/melod/sy_loft.lha
SheLove.png
	ftp://main.aminet.net/pix/back/SheLove.png
djf-1ilove.lzh
	ftp://main.aminet.net/mods/med/djf-1ilove.lzh
inf_love.zip
	ftp://hornet.scene.org/pub/demosdemos/1996/i/inf_love.zip
TrueLove.trd.zip
	https://wos.meulie.net/pub/sinclair/trdos/games/t/TrueLove.trd.zip
mod.WhatIsLove.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.WhatIsLove.lha
hmr_love.lha
	ftp://main.aminet.net/mods/med/hmr_love.lha
MyLove.lha
	ftp://main.aminet.net/mods/demo/MyLove.lha
LostLove.jpg
	ftp://main.aminet.net/pix/trace/LostLove.jpg
mod.LoveYouRight.readme
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1994/mod.LoveYouRight.readme
del4_fin.zip
	ftp://hornet.scene.org/pub/demosdemos/1998/d/del4_fin.zip
tfa-love.zip
	ftp://hornet.scene.org/pub/demosdemos/1995/t/tfa-love.zip
CrazyLove.scr
	https://wos.meulie.net/pub/sinclair/screens/load/c/scr/CrazyLove.scr
StrangeLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/s/StrangeLove.gif
w-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1995/w/w-love.zip
perklove.lha
	ftp://main.aminet.net/mods/misc/perklove.lha
bnz_llb.lha
	ftp://main.aminet.net/demo/40k/bnz_llb.lha
mod.LoveStinks.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1992/mod.LoveStinks.lha
okean.zip
	ftp://hornet.scene.org/pub/demosdemos/1995/o/okean.zip
love_anarchy.dms
	ftp://main.aminet.net/demo/mega/love_anarchy.dms
mod.GloryOfLove.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.GloryOfLove.lha
LoveOracle.jpg
	https://wos.meulie.net/pub/sinclair/games-inlays/l/LoveOracle.jpg
mod.What_Is_Love.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/JogeirLiljedahl/mod.What_Is_Love.lha
WorldOfLove.lha
	ftp://main.aminet.net/mods/4mat/WorldOfLove.lha
LoveJoy's Preparation for the GMAT - Programs (boot disk).dsk
	ftp://ftp.apple.asimov.net/pub/apple_II/images/educational/LoveJoy's Preparation for the GMAT - Programs (boot disk).dsk
LoveOracle.tzx.zip
	https://wos.meulie.net/pub/sinclair/games/l/LoveOracle.tzx.zip
yourlove.lha
	ftp://main.aminet.net/mods/jorma/yourlove.lha
Project_love.lha
	ftp://main.aminet.net/mods/med/Project_love.lha
flp-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1995/f/flp-love.zip
spirit-of-love.lha
	ftp://main.aminet.net/mods/fox2/spirit-of-love.lha
LadyInLove.scr
	https://wos.meulie.net/pub/sinclair/screens/load/l/scr/LadyInLove.scr
LoveAffair.tgz
	ftp://ftp.funet.fi/pub/amiga/demos/PARTIES/Party96/4Ch-Music/LoveAffair.tgz
ShiningLove.lha
	ftp://main.aminet.net/mods/demo/ShiningLove.lha
LoveMeBaby.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/l/LoveMeBaby.gif
LoveDungeon_1.0_exe.lha
	ftp://main.aminet.net/game/actio/LoveDungeon_1.0_exe.lha
TrickLove.lha
	ftp://main.aminet.net/mods/synth/TrickLove.lha
deeplove.lha
	ftp://main.aminet.net/mods/stame/deeplove.lha
ColorWaves.jpg
	ftp://main.aminet.net/pix/imagi/ColorWaves.jpg
StrangeLove.gif
	https://wos.meulie.net/pub/sinclair/screens/load/s/gif/StrangeLove.gif
united_frequen.lha
	ftp://main.aminet.net/mods/sbc/united_frequen.lha
intstate.zip
	ftp://hobbes.nmsu.edu/pubmultimedia/wave/wav/intstate.zip
LoveAffair.readme
	ftp://ftp.funet.fi/pub/amiga/demos/PARTIES/Party96/4Ch-Music/LoveAffair.readme
ILoveTrash.lha
	ftp://main.aminet.net/mods/smpl/ILoveTrash.lha
a_love98.lha
	ftp://main.aminet.net/mods/misc/a_love98.lha
LoveQuiz2.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/l/LoveQuiz2.gif
mod.TangoLoveSong.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.TangoLoveSong.lha
TrueLove.scr
	https://wos.meulie.net/pub/sinclair/screens/load/t/scr/TrueLove.scr
Tomakelove.lha
	ftp://main.aminet.net/mods/tp96/Tomakelove.lha
mod.love_me_tender.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/channel4/mod.love_me_tender.lha
LoveLand.lha
	ftp://main.aminet.net/mods/voice/LoveLand.lha
sky_love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1995/s/sky_love.zip
Secrethouse.lha
	ftp://main.aminet.net/mods/misc/Secrethouse.lha
ILoveTheNight.frm.readme
	ftp://ftp.funet.fi/pub/amiga/audio/misc/ftm-modules/ILoveTheNight.frm.readme
Pussy-LoveStoryFromTitanic.txt
	https://wos.meulie.net/pub/sinclair/games-info/p/Pussy-LoveStoryFromTitanic.txt
vd-love.lha
	ftp://main.aminet.net/demo/aga/vd-love.lha
Love.lha
	ftp://main.aminet.net/mods/ooze/Love.lha
apx-ili.lha
	ftp://main.aminet.net/pix/trace/apx-ili.lha
RomperRoomsILoveMyAlphabet_Back.jpg
	https://wos.meulie.net/pub/sinclair/games-inlays/r/RomperRoomsILoveMyAlphabet_Back.jpg
WinterLove.lha
	ftp://main.aminet.net/mods/evrim/WinterLove.lha
LoveAffair.lha
	ftp://main.aminet.net/mods/tp96/LoveAffair.lha
mod.WantMyLove.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.WantMyLove.lha
LoveMeBaby.tap.zip
	https://wos.meulie.net/pub/sinclair/demos/l/LoveMeBaby.tap.zip
Pussy-LoveStoryFromTitanic.scr
	https://wos.meulie.net/pub/sinclair/screens/load/p/scr/Pussy-LoveStoryFromTitanic.scr
Love-Hardcore.lha
	ftp://main.aminet.net/mods/techn/Love-Hardcore.lha
IWannaKnowWhatLoveIs.med.readme
	ftp://ftp.funet.fi/pub/amiga/audio/modules/Roberto/IWannaKnowWhatLoveIs.med.readme
Fairlight.LOVE.readme
	ftp://ftp.funet.fi/pub/amiga/demos/Fairlight/Fairlight.LOVE.readme
CrazyLove.gif
	https://wos.meulie.net/pub/sinclair/screens/load/c/gif/CrazyLove.gif
GetANewLife.lha
	ftp://main.aminet.net/mods/nork/GetANewLife.lha
ILoveYou.lha
	ftp://main.aminet.net/mods/misc/ILoveYou.lha
LadyInLove.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/l/LadyInLove.gif
ms-elove.lha
	ftp://main.aminet.net/mods/melod/ms-elove.lha
Arianna.lha
	ftp://main.aminet.net/mods/pro/Arianna.lha
lovestik.zip
	ftp://hobbes.nmsu.edu/pubmultimedia/music/mod/unknown/lovestik.zip
mod.WhoNeedsLoveLikeThat.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.WhoNeedsLoveLikeThat.lha
phd_love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1998/p/phd_love.zip
LadyInLove.gif
	https://wos.meulie.net/pub/sinclair/screens/load/l/gif/LadyInLove.gif
ArtificialLove.lha
	ftp://main.aminet.net/mods/pro/ArtificialLove.lha
RomperRoomsILoveMyAlphabet.tzx.zip
	https://wos.meulie.net/pub/sinclair/games/r/RomperRoomsILoveMyAlphabet.tzx.zip
IWannaKnowWhatLoveIs.med.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/Roberto/IWannaKnowWhatLoveIs.med.lha
LoveOracle.gif
	https://wos.meulie.net/pub/sinclair/screens/in-game/l/LoveOracle.gif
I_LoveTheNight.lha
	ftp://main.aminet.net/mods/8voic/I_LoveTheNight.lha
LoveNotWar.mp3
	ftp://main.aminet.net/mods/mpg/LoveNotWar.mp3
mod.LoveFunk.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1993/mod.LoveFunk.lha
TwelveDaysOfChristmasThePart1-CourseOfTrueLove.z80.zip
	https://wos.meulie.net/pub/sinclair/games/t/TwelveDaysOfChristmasThePart1-CourseOfTrueLove.z80.zip
VR-Love.jpg
	ftp://main.aminet.net/pix/trace/VR-Love.jpg
love.rsid
	ftp://ftp.modland.com/pub/modules/RealSID/A-Man/love.rsid
pls_love.zip
	ftp://hornet.scene.org/pub/demosgraphics/images/1997/p/pls_love.zip
mod.AKindOfLove.readme
	ftp://ftp.funet.fi/pub/amiga/audio/modules/Party94/mod.AKindOfLove.readme
srh_love.lha
	ftp://main.aminet.net/demo/intro/srh_love.lha
Love.lha
	ftp://main.aminet.net/mods/techn/Love.lha
RomperRoomsILoveMyAlphabet_Front.jpg
	https://wos.meulie.net/pub/sinclair/games-inlays/r/RomperRoomsILoveMyAlphabet_Front.jpg
Robotic.mpg
	ftp://main.aminet.net/mods/elbie/Robotic.mpg
jd-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1996/j/jd-love.zip
RomperRoomsILoveMyAlphabet.scr
	https://wos.meulie.net/pub/sinclair/screens/load/r/scr/RomperRoomsILoveMyAlphabet.scr
WBprefs.lha
	ftp://main.aminet.net/pix/mwb/WBprefs.lha
secret.lzh
	ftp://main.aminet.net/mods/panik/secret.lzh
mod.AKindOfLove.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/Party94/mod.AKindOfLove.lha
lovereal.lha
	ftp://main.aminet.net/mods/pro/lovereal.lha
mod.DontNeedYourLove.lha
	ftp://ftp.funet.fi/pub/amiga/demos/PARTIES/Party93/music/mod.DontNeedYourLove.lha
mod.LoveYouRight.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/misc/1994/mod.LoveYouRight.lha
Pussy-LoveStoryFromTitanic.jpg
	https://wos.meulie.net/pub/sinclair/games-inlays/p/Pussy-LoveStoryFromTitanic.jpg
Padre_IloveYou.lha
	ftp://main.aminet.net/mods/8voic/Padre_IloveYou.lha
GalacticLove.lha
	ftp://main.aminet.net/mods/nork/GalacticLove.lha
ILoveTheNight.ftm.lha
	ftp://ftp.funet.fi/pub/amiga/audio/misc/ftm-modules/ILoveTheNight.ftm.lha
jzd-love.zip
	ftp://hornet.scene.org/pub/demosmusic/songs/1997/j/jzd-love.zip
EternalLove.lha
	ftp://main.aminet.net/mods/med/EternalLove.lha
RomperRoomsILoveMyAlphabet.gif
	https://wos.meulie.net/pub/sinclair/screens/load/r/gif/RomperRoomsILoveMyAlphabet.gif
Micro Tennis (1983)(DaTaBioTics)(Part 2 of 2)[AKA Love Tennis].zip
	ftp://ftp.whtech.com/emulators/mess/old/Complete MESS Geneve emulation/mess/software/ti99_4a/Micro Tennis (1983)(DaTaBioTics)(Part 2 of 2)[AKA Love Tennis].zip
mod.time_for_love.lha
	ftp://ftp.funet.fi/pub/amiga/audio/modules/nuke-anarchy/mod.time_for_love.lha
294 of 315 files.
```

#### webradio_dump

```bash
source ~/net.lisias.retro/runtime/bin/activate
cd ~/net.lisias.retro/sdk/src/python
python -m examples.console.webradio_dump
```

Uma pequena prova de conceito com o `asyncio` do Python 3.4. Conecta com o WebRadio da Confederação e faz um *dump* do stream de dados.

É possível salvar o *streaming* em arquivo redirecionando a saída para um arquivo:

```bash
source ~/net.lisias.retro/runtime/bin/activate
cd ~/net.lisias.retro/sdk/src/python
python -c examples.console.webradio_dump > arquivo.dump
```

O formato de áudio pode ser pescado dos cabeçalhos do `IceCast2`. Com os dados em arquivo, é possível envelopar num arquivo mp3 (você precisa criar o cabeçalho, não adianta simplesmente renomear o arquivo do *stream*)

Se você tiver o `lame` instalado, pode gambiarrar um player meia boca com os seguintes comandos:

```bash
source ~/net.lisias.retro/runtime/bin/activate
cd ~/net.lisias.retro/sdk/src/python
python -m examples.console.webradio_dump | lame --decode --mp3input - /dev/dsp
	(ou seja lá como é o device de audio na sua máquina)
```

#### webradio 2 e 3

Instalar as dependências destes carinhas é um saco, e são as mesmas para ambos, então concentrei aqui as informações.

Antes de rodar pela primeira vez, você precisa instalar as dependências:

Num Mac com MacPorts:
```bash
source ~/net.lisias.retro/runtime/bin/activate
sudo port install portaudio
pip install --global-option='build_ext' --global-option='-I/opt/local/include' --global-option='-L/opt/local/lib' pyaudio
```

Num Linux (Fedora - usar `apt-get` e `-dev` ao invés de `-devel` se Debian):
```bash
source ~/net.lisias.retro/runtime/bin/activate
sudo yum install portaudio portaudio-devel
pip install pyaudio
```

Se tudo o mais falhar (no OLPC foi um saco!):
```bash
source ~/net.lisias.retro/runtime/bin/activate
# Instalar o poraudio (com headers) como manda a sua distribuição
cd source ~/net.lisias.retro/runtime/
mkdir src
git clone http://people.csail.mit.edu/hubert/git/pyaudio.git pyaudio
cd pyaudio
cat setup.py | sed "/long_description=__doc__/long_description='duh'/g" > setup_hack.py
mkdir docs
python setup_hack.py install
```

Todos os comandos acima precisam ser executados sem falha, incluindo o `mkdir docs`, senão o setup vai falhar.

Ufa! :-)

##### webradio2

Mais um teste de conceito, desta vez jogando o *streaming* burramente para o PyAudio tocar na placa de som. Prepare seus ouvidos, será desagradável. :-)

Não roda em Python2 devico à I/O Assíncrona. Não deve ser trabalhoso fazer *backporting*, ver o `webradio_dump`.

Se você achar um streaming de PCM, este programa pode até ser útil. :-)

```bash
source ~/net.lisias.retro/runtime/bin/activate
cd ~/net.lisias.retro/sdk/src/python
python -m examples.console.webradio2
```

##### webradio3

Este sim! :-)

Através de uma mutreta esperta (pra falar a verdade, o bom e velho conceito UNIX na sua melhor forma), o *stream* é entubado (*piped*) para um processo externo rodando o `lame` que por sua vez entuba os dados decodificados de volta para o nosso processo, que por sua vez alimenta o PyAudio.

Funciona bem 99.99% do tempo - mas dependendo da carga do computador (e do seu poder de processamento) aparece um silêncio ou outro (já que a CPU segurou o processo do `lame`).

No geral, este exemplo apresenta algumas boas soluções para problemas relativamente complexos, como I/O Assíncrona; bufferização transparente; *multithreading*; e por fim *live decoding* e encadeamento de *streams*.

```bash
source ~/net.lisias.retro/runtime/bin/activate
cd ~/net.lisias.retro/sdk/src/python
python -m examples.console.webradio3
```
