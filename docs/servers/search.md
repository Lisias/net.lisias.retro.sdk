net.lisias.retro.file.search.WS
===============================

Documentação do W/S Repo Search (bleh... que nome dou pra isso?)

Não botei autenticação nem controle - logo, sim, o RPi está bem suscetível à um DoS. Testes empíricos demonstraram que o meu upstream doméstico é uma limitação, mas também um *deterrance* por efeito colateral. Um upstream decente, e o W/S efetivamente sufocaria o resto da máquina trabalhando nas responses. Não, não tem cache (isso vai ficar por conta do NGINX, quando isso subir pro retro.lisias.net - aliás, o QoS vai ficar por conta dele também).

No momento, rodam em um [Raspberry Pi 2011 Model A (with eth)](https://www.adafruit.com/product/1344), em um [Raspberry Pi 2011.12 Model B](https://www.adafruit.com/products/998) e também num [Raspberry Pi 2014 Model B+](https://www.adafruit.com/products/1914).

A idéia original era montar serviços que rodassem em computadores de recursos mínimos, daqueles que se leva para os Encontros de Retrocomputação para apoio ao evento. Usei um RPi porque era o que tinha na mão, mas isso vai rodar com certeza (talvez até melhor) naqueles Thin Client de segunda mão que tão pipocando na Sta Efigênia (tem de 50 a 200 reais).

Mas os serviços estão crescendo (3:-)) mais rápido que a capacidade do meu bolso em comprar brinquedos novos, e enquanto procurava alguma coisa barata que pudesse expandir o setup local da Confederação, topei com um [Dell 1425SC](http://www.stikc.com/Catalog/PowerEdge-SC1425) mais barato que um Raspberry Pi 3 com SD Card. Bom, abraçado a causa das Ações Afirmativas, este monstrinho será o 4º Raspberry Pi da Confederação. ***Atente seu privilégio***, esta máquina tem todo o direito de se sentir um Raspberry Pi se ela assim o custar!! =D =D =D

## Serviço REST W/S ##

A Jóia da Coroa. :-) Implementado em [Python 3](https://docs.python.org/3/) . Roda bem no CPython, mas o [PyPy3 5.5.0](http://pypy.org/download.html) faz o mesmo trabalho 3.75 vezes mais rápido. Mas como o Pypy3 ainda tá em alpha, tô servindo no CPython mesmo.

Uma fuçada rápida nas páginas já diz tudo o que precisa saber para usar o serviço, mas segue uma descrição rápida:

Dado o endereço base, que no momento é http_//home.lisias.net_8081/ (trocar _ por :), o request sempre segue a forma

	/<algumacoisa>.<formatodesejado).

Não usar o `<formatodesejado>` implica numa response em `text/plain` (formato `.txt`, adequado para consultas manuais e debugging). No momento, reconhece `.json` e `.jsonp` (json com padding, para furar cross-domain scripting). Sempre GET, e os eventuais parâmetros da Query seguem a rfc3986 (ou pelo menos, é o que o [Bottle](http://bottlepy.org/docs/dev/) me disse).

Se faz um request que existe mas com dados inconsistentes, recebe um 200 com um Response explicando o erro. Se você faz um request que não existe, recebe um 404. Se você faz um request que existe mas com dados inválidos, leva um 500 na orelha e fica por isso mesmo.

Toda resposta retorna um campo booleano chamado `error`. Se `False`, `success_msg` tem uma mensagem de confirmação (normalmente um contador) e os demais campos são o payload da Response - dependente de Request. Se houve um erro, `error_msg` dá a razão por extenso, e `error_code` um identificador único no sistema do erro, para automatizar tratamento de erro.

Uma resposta de sucesso **pode** retornar um campo `timetaken_in_secs` onde o tempo gasto, em segundos, no processamento do request é informado num valor *float*. Responses com coleções **podem** ter este dado em cada item da coleção. Este campo é opcional, e pode ou não estar presente - incluindo entre um Request e outro do mesmo server.

### Requests implementados: ###

#### `/version.<fmt>` ####
retorna a identificação da aplicação e a versão corrente. Bom para saber se o serviço tá de pé (ping). Retorna os formatos reconhecidos pelo W/S (bom para saber o que vc pode usar na hora, já que novos formatos virão).

#### `/metadata.<fmt>` ####
retorna os *archives* reconhecidos pelo W/S (asimov, wos, etc). Na medida que novos *archives* são adicionados, vai-se atualizando esta chamada.

Exemplo:

* Request: http://home.lisias.net:8083/metadata

* Response:

	```
	net.lisias.retro Confederation of Retrocomputing Micro Services.

	REQUEST: GET http://home.lisias.net:8081/metadata

	error : False
	success_msg : 'Archives:4'
	archives : ['asimov', 'freedos', 'arnold', 'wos']


	To get help : http://service.retro.lisias.net/	```

#### `/<archive>/metadata.<fmt>` ####
retorna as entidades reconhecidas por este *archive*, bem como seus verbos de ação. Cada entidade vem numa tupla, com o singular e com o plural. Se o Request retorna *UMA* entidade, use o singular. Se o Request retorna uma *coleção*, use o plural. Os verbos também vêm em tuplas, com o seu nome e uma lista das entidades que reconhecem.

Exemplo:

* Request: http://home.lisias.net:8083/asimov/metadata

* Response:

	```
	net.lisias.retro Confederation of Retrocomputing Micro Services.

	REQUEST: GET http://home.lisias.net:8083/asimov/metadata

	error : False
	success_msg : 'asimov Entities: 6 ; Verbs: 2'
	archive : 'asimov'
	data :
	verbs :
		search : ('files',)
		list : ('keywords', 'classes', 'tags', 'file_types', 'fn_fragments')
	entities : (('file', 'files'), ('keyword', 'keywords'), ('class', 'classes'), ('tag', 'tags'), ('file_type', 'file_types'), ('fn_fragment', 'fn_fragments'))


	To get help : http://service.retro.lisias.net/
	```

Estas duas chamadas serão importantes na Confederação, uma vez que cada W/S será capaz de explicar o que entende e como acessar, e o agregador poderá fazer o roteamento. Também é legal para fazer um cliente mais espertinho (ao contrário das minhas páginas de exemplo, que são burrinhas de dar dó).

#### Verbos implementados ####

Uma vez escolhido o `<archive>`, a mecânica é sempre a mesma:

`/<archive>/<entitiy_plural>/<verb>.<fmt>?<parms>` (onde parms é rfc3986 )

##### `/<archive>/<entitiy_plural>/list.<fmt>` #####

para obter todos os valores de uma entidade.

Por exemplo, `/asimov/classes/list.jsonp?callback=callback_16` vai retornar um JSONP com todas as classes reconhecidas pelo *archive* asimov:

`callback_16({"success_msg": "Classes:7", "classes": [["incoming", 3], ["documentation", 5991], ["asm", 1], ["images", 20460], ["utility", 187], ["unsorted", 624], ["emulators", 513]], "error": false});`

Cada valor é uma tupla com dado e incidência - no exemplo acima, há 20460 tuplas cadastradas para a Classe "images".

##### `/<archive>/<entity_plural>/search.<fmt>` #####

Até agora, só brincamos de metafísi... uh... metadados :-) . A razão de ser do W/S é o verbo que segue:

`/<archive>/<entity_plural>/search.<fmt>?keywords=<list>;classes=<list>;tags=<list>;file_types=<list>;fn_fragments=<list>;option=<OPTION>;fields=<FIELD_LIST>`

Ela faz a busca pelas entidades (no momento, apenas "files") dado um critério de busca. Os valores entre <> podem ser:

* <list> : uma lista de palavras separadas por vírgula (`,`)
* <OPTION>: uma das opções que seguem:
	+ MINIMAL : retorna um subset mínimo de cada *datum* (unidade de dado).
		+ Para este *request*, retorna apenas a `url`.
	* SHORT : retorna um subset reduzido de cada *datum*.
		+ Para este *request*, retorna (quando disponível) `size`, `description` (abreviado se extender 2K), `url`, `dirs`, `filename`, `extension`, `date` e `fsid`.
		+ é o *default* quando omitido
	* FULL : retorna o set completo do *datum*.
		+ Mesmos campos de SHORT, mas sem abreviar o `description`.
* <FIELD_LIST>: uma lista de campos separados por vírgula (`,`). Se omitido, `option` define os campos a serem usados.
	+ `fsid` - identificador de "*file system*" , necessário para identificar a fonte do arquivo em buscas muti-*archive*. (útil para o serviço de *mirrors*)
	+ `url` - URL canônica (ou seja, para o endereço oficial) do arquivo.
	+ `dirs` - Lista ordenada de (sub)diretórios, já separados, para avaliação e filtro no *client side*
	+ `filename` - nome do arquivo, sem extensão.
	+ `extension` - extensão do nome do arquivo.
	+ `alias` - nome *human friendly* do arquivo, oferecido por alguns repositórios.
	+ `size` - tamanho do arquivo, em bytes. Não disponível em todos os repositórios.
	+ `date` - dia da última modificação do arquivo. Não disponível em todos os repositórios. A hora não é fornecida por questões técnicas (as listagens do FTP só dão hora quando o arquivo foi modificado no ano corrente).
	+ `description` - descrição do arquivo, quando disponível. A cláusula `option` acima é reconhecida, limitando o tamanho do dado para 255 bytes, 2K e ilimitado respectivamente. Note que alguns repositórios são bastante generosos na descrição do arquivo (com casos de vários megabytes!).

Uma busca para o scene_org segue:

* Request: http://home.lisias.net:8090/scene_org/files/search.txt?tags=future_crew,music;keywords=assembly;option=MINIMAL;fields=fsid,url,description

* Response:

	```
	net.lisias.retro Confederation of Retrocomputing Micro Services.

	REQUEST: GET http://localhost:8081/scene_org/files/search.txt?tags=future_crew,music;keywords=assembly;option=MINIMAL;fields=fsid,url,description

	error : False
	files : [{'description': 'Assembly 1994 Invitation Intro Music\nby Purple Motion / Future Crew', 'fsid': 'scene_org', 'url': 'ftp://ftp.scene.org/demos/groups/future_crew/music/purple_motion/pm_asm94.zip'}, {'description': 'Sundance\n\nPurple Motion of The Future Crew\n\nThis is my contribution song for\nthe Assembly PC music compo', 'fsid': 'scene_org', 'url': 'ftp://ftp.scene.org/demos/groups/future_crew/music/purple_motion/sundance.zip'}]
	timetaken_in_secs : 0.0038279999999986103
	cardinality : 2
	success_msg : 'Files:2.'


	To get help : http://service.retro.lisias.net/	```

#### Requests "Confederativos" ####

Para gerenciamento do Pool de Proxies (a "Confederação"), o seguinte request foi implementado.

É o único **POST** em todo o Serviço.

##### `/announce.<fmt>` #####

É usado por um **proxy.WS** para anunciar que este serviço foi incluído na sua Unidade Confederativa.

Não é necessário tomar nenhuma atitude, mas espera-se que o Provider inclua o endereço mencionado no **Referer** na sua lista de proxies em que está registrado - para sair desregistrando durante o shutdown do serviço.


*Archives*
-----------

### Online

*Archives* e seus repositórios. O Primeiro é o que está sendo servido no momento. As demais URLs serão servidas quando implementar suporte para mirrors.

*Zombie* repos são repositórios que não são mais atualizados (ou não o são há mais de uns 5 anos), e portanto não precisam checar cache após a primeira carga - se o arquivo existe, já veio, se não existe, não adianta tentar de novo.

*Dead* repos são repositórios que não existem mais, e estão disponíveis apenas por espelhos (oficiais ou não). São tratados da mesma forma que os *Zombie*.

*Alive* são repos que não são nem *Zombie* nem *Dead*. :-)

*Brute Force* são repositórios mantidos por amadoras =/ que não sabem a importância de um `index.txt` , `all_files` ou `ls-LR` . Para estes repos, o all_files é construído por força bruta, literalmente varrendo o FTP do cara por inteiro e construindo o arquivo local. Repositórios vivos com esta característica são especialmente contra-produtivos, pois a única forma realmente segura de checar novidades é varrer o ftp o tempo todo!

Para este último problema (repositórios vivos com bruta força), uma heurística foi adotada na esperança de ser útil: apenas o diretório raiz do reposítório é checado duranta a inicialização - se houver qualquer mudança (arquivo novo ou faltante, ou um arquivo com timestamp ou tamanho diferente)
, o cache local é considerado invalido.

Como *zombie* entende-se repositórios que estiveram *dead* por anos, e tiveram **uma ou duas** updates nos últimos 12 meses. Estão cadastradas como *deads* até eu descobrir (se existir) uma forma barata de checar novidades.

* **amigascne** - alive
	* ftp://ftp.amigascne.org/pub/amiga/
* **aminet** - alive
	* ftp://main.aminet.net/
* **arnoldC64** C64 - alive - brute_force
	* ftp://arnold.c64.org/pub/
* **asimov** - alive
	* ftp://ftp.apple.asimov.net/pub/apple_II/
* **freedos** - alive
	* ftp://ftp.ibiblio.org/pub/micro/pc-stuff/freedos/
* funet -- all zombies
	* O repo da funet é separado por área de interesse, e cada "sub-repo" é totalmente diferente do outro. Então dividiu-se em vários *Archives* independentes.
	* Alguns "sub-repos" saíram do ftp da Funet e foram para outros servers. Mas como continuam do mesmo jeito, mantive a nomenclatura.
		* **funet.amiga**
			* ftp://ftp.funet.fi/pub/amiga/
		* **funet.atari**
			* ftp://ftp.funet.fi/pub/atari/
		* **funet.msx**
			* ftp://ftp.funet.fi/pub/msx/ - gone
			* http://www.msxarchive.nl/pub/msx/ - current cannonical repsitory
* **gaby** - zombie
	* ftp://ftp.gaby.de/pub/ - gone
	* ftp://ftp.gaby.de1.biz/2/268517_81539/pub - current cannonical repsitory
* **garbo** - dead
	* ftp://garbo.uwasa.fi/pc/garbo/ - gone
	* ftp://major.butt.care/mirrors/garbo.uwasa.fi/
* **hobbes** - alive
	* ftp://hobbes.nmsu.edu/pub/
* **hornet** - dead
	* ftp://ftp.hornet.org/pub/demos - gone
	* ftp://hornet.scene.org/pub/demos
* **modland** - alive
	* ftp://ftp.modland.com/pub/modules/
* **metalab.pdp** - alive
	* ftp://ftp.metalab.unc.edu/pub/academic/computer-science/history/
* NVG:
	* **nvg.bbc** - alive
		* ftp://ftp.nvg.ntnu.no/pub/bbc/
	* **nvg.cpc** - alive
		* ftp://ftp.nvg.ntnu.no/pub/cpc/
	* **nvg.hardware** - zombie
		* ftp://ftp.nvg.ntnu.no/pub/hardware/
	* **nvg.samcoupe** - alive
		* ftp://ftp.nvg.ntnu.no/pub/sam-coupe/
	* **nvg.sinclair** - zombie
		* ftp://ftp.nvg.ntnu.no/pub/sinclair/
	* **nvg.sounds** - zombie
		* ftp://ftp.nvg.ntnu.no/pub/sounds/
	* **nvg.vms** - zombie
		* ftp://ftp.nvg.ntnu.no/pub/vms/
* **paduaC64** - alive
	* ftp://ftp.padua.org/pub/c64/
* **scene_org** - alive
	* ftp://ftp.scene.org/pub/
* **simtel** - dead
	* ftp://ftp.sunet.se/mirror/archive/ftp.sunet.se/pub/simtelnet/
		* Por falha do repo (ls-LR desatualizado)
			* não indexa os seguintes diretórios:
				* ./mac
				* ./mobile
				* ./pal
			* E outros que estão vazios no ls-lR mas tem um arquivo ou outro no diretório. =/
	* ftp://major.butt.care/mirrors/ftp.simtel.net/
		* Similar ao anterior, sem ls-LR
	* ftp://ftp.sunet.se/mirror/archive/ftp.sunet.se/pub/simtelnet/
		* é mais completo, mas não tem ls-LR
		* Um Crawler FTP inteligente é RiP.
	* ftp://ftp.funet.fi/pub/msdos/Simtel/pub/simtelnet/
		* idem, idem.
* **tv-dog** - alive
	* ftp://ftp.oldskool.org/pub/tvdog/tandy1000/
* **whtech** - alive
	* ftp://ftp.whtech.com/
* **wos** - dead
	* ftp://ftp.worldofspectrum.org/pub/sinclair/ - gone
	* https://wos.meulie.net/pub/sinclair/ - gone
	* DAMN!!
* **x2ftp** - dead
	* ftp://x2ftp.oulu.fi/pub/ - gone
	* ftp://ftp.lanet.lv/pub/mirror/x2ftp/

### Work in Progress

Vou documentar isso aqui porque preciso guardar esta lista em algum lugar, senão me perco. :-)

* FUNET
	* ftp://ftp.zimmers.net/pub/ (cbm, cmb-pc, cpm, geos, windows)
	* outros:
		* ftp://ftp.funet.fi/pub/msdos/
		* ftp://ftp.funet.fi/pub/os2/
		* ftp://ftp.funet.fi/pub/NeXT/
		* ftp://ftp.funet.fi/pub/mac/
	* Referencia:
		* http://www.nic.funet.fi/pub/
		* ftp://ftp.funet.fi/pub/
	* Cada repo é diferente, preciso implementar um Adapter diferente.
	* Na agulha
		* Nenhum no momento. Sugestões?
* Por fim, uma pancada de mirrors:
	* ftp://major.butt.care/mirrors/
	* http://www.classiccmp.org/cpmarchives/
	* ftp://ftp.bu.edu/mirrors/
	* ftp://ftp.gamers.org/pub/
	* http://www.sundby.com/mirror/


Endereço atual
--------------

http://search.lisias.net/retro


*Interesting Issues Solved*
----------------------------

Algumas issues foram de doer. A pior delas foi performance. Numa tentativa (quase) desesperada de melhorar a performance da carga dos dados no RPi, dei uma chance pro PyPy3 5.5.0 (ainda alfa). Me estrepei, porque o tempo de carga piorou uma barbaridade (VINTE VEZES, pô!!) - mas a bateria de testes do WebService ficou QUATRO VEZES MAIS RÁPIDA.

Bom, vale a pena perseguir isso. Então faz cProfile daqui, faz cProfile de lá... E descobri que o problema se concentrava em DUAS funções (todo o resto era acelerado uma barbaridade no PyPy!). Uma era minha (que explodia em 200 vezes em relação ao CPython), a outra é o str.translate (que só explodia 10).

Deste último não tem o que fazer, o str do PyPy3 é lerdo mesmo. Mas nem era o pior problema. Dureza era o que *EU* estava fazendo que no PyPy que em geral tem uma peformance cavalar, pontualmente neste caso virava uma carroça de bois, com os ditos cujos mancos, com joanete e hemorróidas.

Pelo menos a função tinha só 7 linhas - a análise combinatória era baixa. :-)

### dict.keys()]

Fazendo Test Beds da função em questão, um para cada cenário, cheguei à uma conclusão reveladora =P : eu tava fazendo bobagem! Vindo do Java, eu achei uma boa idéia checar se uma chave já era usada num Dict assim:

```Python
if key in my_dict.keys():
	do_something()
```

No CPython funciona legalzinho, mas esta maldita linha no PyPy3 fica VINTE VEZES MAIS LERDA. o.O

Fuça daqui, pesquisa dali, xinga a mãe dos caras acolá, eu vi na documentação deles que o Garbage Collector do PyPy3 é bem menos performático que o do CPython (não vou entrar em detalhes).

Daí caiu a ficha! No Python3, a função `dict.keys()` retorna um objeto do tipo dict_keys, que é no fundo um iterator, e não uma lista como eu estava acostumado!!!! Daí que para funcionar como eu esperava, o Python interava tudo numa list e dava pro `if` fazer o trabalho dele e depois jogava a lista fora. Em outras palavras, olha a memória enchendo de lixo aí.... =/

Daí é que me toquei que a mesmo resultado se obtêm (e de forma mais pythomática =P ) assim:

```Python
if key in my_dict:
	do_something()
```

Fazendo profilling no CPython, não fez diferença NENHUMA. Imagino que os caras do CPython tenham de alguma forma feito uma otimização marota :-) neste "idioma", mas no PyPy3 a função passou a seguir a norma das demais e ficou 4 vezes mais rápida!

### str.translate

Esta aqui me quebrou as pernas. Apesar de não causar um dano tão feio quanto ao pepino da dict_keys, também mordia um bocado. Me enchi de coragem e ataquei este problema também.

Eu não sei exatamente qual era o gargalo, mas estava na implementação da `str.translate` do PyPy3. Lendo a documentação, vi que a str foi implementada em Python puro, não era uma interface com uma biblioteca em C. Num chute calculado, apostei também no Garbage Collector, e procurei uma forma de fazer o mesmo serviço sem apelar para as funções da str.

Cheguei no *pattern*:

```Python
	parts = []
	for c in [ c for c in string if c not in Translate.SET_OF_CHARS ]:
		parts.append(c)
	return ''.join(parts)
```

Para substituir um string.translate cuja tabela seja `{ord(character):None for character in SET_OF_CHARS}`.

Outro *pattern* que usei foi:

```Python
	parts = []
	for c in string :
		parts.append(c if c in SET_OF_CHARS else " ")
	return ''.join(parts)
```

Para substituir um string.translate cuja tabela seja `{ord(character):" " for character in SET_OF_CHARS}`.

A razão é que ao invés de concatenar strings, que gera lixo pra cacete (e então o GC, ponto fraco do PyPy3, nos morde os calcanhares), o ''.join não gera quase nenhum. Isto está documentado no [FAQ do PyPy](http://pypy.org/performance.html).

Só que, e de novo, "O remédio do Pato é veneno pro marreco." =/ Isso virtualmente f*deu a performance no CPython. (sigh).

Abrir mão do PyPy3 à esta altura do campeonato não me agradava nem um pouco, mas não dá pra abrir mão do CPython tampouco. O jeito foi encapsular estas chamadas dentro duma Fachada, que em tempo de carregamento checa o runtime corrente e gera funções dinamicamente:

```Python
class Translate:
	'''
	Class needed to abstract CPython and PyPy performance differences
	'''
	SET_OF_CHARS = set([chr(i) for i in some_criteria()])
	CHAR_MAP = {ord(character):" " for character in SET_OF_CHARS}

	if 'PyPy' == platform.python_implementation():
		@staticmethod
		def do_some_translation(string : str):
			parts = []
			for c in string:
				parts.append(" " if c in Translate.SET_OF_CHARS else c)
			return ''.join(parts)
	else:
		@staticmethod
		def do_some_translation(string : str):
			return string.translate(Translate.CHAR_MAP)
```

E então sair usando `Translate.do_some_translation()` onde eu precisava. Óbvio que para cada CHARMAP, tinha que ter uma função equivalente, a minha Fachada tem umas 5 funções diferentes para cada runtime.

E foi isso. Estes dois pequenos =P *hacks* resolveram meus problemas de performance no PyPy3 sem ferrar com o CPython.

### Resultdo final:

#### Carga de dados

* CPython
	-  Archive  | Carga Desktop CPython | Carga RPi CPython
	:-----------|----------------------:|---------------------:
	asimov      | 4.386759 secs         | 135.279584 secs
	aminet      | 28.205735 secs        | 574.530708 secs
	msxarchive  | 2.243895 secs         | 65.801364 secs
	whtech      | 7.771284 secs         | 136.619781 secs
	wos         | 18.344768 secs        | 537.528379 secs
				| 60.952441 secs        | 1449.759815 secs

* PyPy3
	-  Archive  | Carga Desktop PyPy3   | Carga RPi PyPy3
	:-----------|----------------------:|-------------------:
	asimov      | 4.874308 secs         | 142.395216 secs
	aminet      | 26.514219 secs        | 487.731522 secs
	msxarchive  | 2.951330 secs         | 80.682657 secs
	whtech      | 6.604925 secs         | 153.990286 secs
	wos         | 17.008360 secs        | 498.197506 secs
				| 57.953142 secs        | 1362.997186 secs

* Diff
	- Máquina   | CPython          | PyPy3            | Diff
	:----------|-----------------:|-----------------:|-----------------:
	Desktop    | 60.952441 secs   | 57.953142 secs   | 2.999299 secs (~5.1753%)
	RPi        | 1449.759815 secs | 1362.997186 secs | 86.762628 secs (~6.3655%)

Curioso que embora os *Archives* menores tenham comido mais tempo de processamento, o WoS e o AmiNet (que são o dobro dos menores juntos!) foram internalizados em tempo menor (JIT?). Acabou que o tempo de carga total melhorou em todos os casos. Curiosamente, foi justo no RPi que os ganhos percentuais foram melhores.

#### Bateria de Testes do Web Service

Os resultados dos testes de performance da bateria de testes foram:

* Desktop
	-  Archive  | Bateria Teste CPython | Bateria Teste PyPy3  | Diff
	:-----------|----------------------:|---------------------:|---------------------------:
	asimov      | 5.828362 secs         | 1.411942 secs        |  4.416420 secs (412.7904%)
	aminet      | 35.070637 secs        | 8.368545 secs        | 26.702092 secs (419.0768%)
	msxarchive  | 3.875705 secs         | 0.857899 secs        |  3.017806 secs (451.7670%)
	whtech      | 13.370379 secs        | 2.743155 secs        | 10.627224 secs (487.4088%)
	wos         | 18.344768 secs        | 4.563606 secs        | 13.781161 secs (401.9796%)

* RPi
	-  Archive  | Bateria Teste CPython | Bateria Teste PyPy3  | Diff
	:-----------|----------------------:|---------------------:|---------------------------:
	asimov      | 102.643164 secs       | 18.055132 secs       |  84.588032 secs (568.4985%)
	aminet      | 544.698183 secs       | 98.694104 secs       | 446.004079 secs (551.9054%)
	msxarchive  | 66.782441 secs        | 11.754195 secs       |  55.028246 secs (568.1583%)
	whtech      | 238.036534 secs       | 35.420269 secs       | 202.616265 secs (672.0348%)
	wos         | 276.013509 secs       | 57.357741 secs       | 218.655768 secs (481.2140%)

O PyPy3 consistentememnte entregou uma performance superior a 400% em todos absolutamente os casos (depois que, óbvio, eu me livrei das fraquezas dele!). No RPi, esteve acima de 550% em quase todos (menos um) casos.

É muita coisa pra se deixar passar.

Uma vez que a máxima "O remédio do pato normalmente é veneno pro marreco" vale aqui, eu criei fachadas que dinamicamente definem funções em função do ambiente de execução - de forma que eu continuo desenvolvendo em CPython (porque nem todo ambiente de execução tem um PyPy3!), e na hora do runtime as otimizações são usadas automaticamente.

Para se ter uma idéia do problema original, abaixo a tabela de performance antes e depois das **duas** otimizações para o PyPy3. Apenas no Desktop, porque os tempos no RPi são estupidamente grandes para valer a pena obter estes dados (20 a 45 minutos por teste, sem chance). A Bateria de Testes não sofreu nenhuma diferença depois das otimizações (o código que tinha o "bug" era pouco usado, a incidência foi virtualmente nula), de forma que fiz estes testes apenas para a carga.

Archive | Carga Antiga CPython  | Carga Antiga PyPy     | Carga Nova CPython  | Carga Nova PyPy
:------------|----------------------:|----------------------:|--------------------:|-----------------:
asimov      | 6.976422 secs         | 36.006185 secs        | 6.882563 secs       | 6.649997 secs
msxarchive  | 3.339096 secs         | 14.853703 secs        | 3.027886 secs       | 3.828372 secs
wos         | 26.094766 secs        | 436.094230 secs       | 27.642663 secs      | 23.894624 secs
(total)      | 36.410284 secs        | 486.954118 secs       | 37.553112 secs      | 34.372993 secs

Os números falam por si mesmos (*archives* não mencionados não estavam disponíveis durante a transição).
