net.lisias.retro.file.mirror Services
=====================================

## Prefácio

É fato que o tempo passa, e tecnologias tendem à caírem em desuso. Mas a falta de um bom substituto para o bom e velho FTP me leva à temer pelo futuro dos repositórios de dados mais importantes das décadas que se passaram.

Os novos mantenedores não parecem atentar para as limitações das tecnologias substitutas, o que francamente me leva à questionar se a nova geração tem realmente a competência e ou vontade necessárias para manter o legado - pior, alguns parecem querer exatamente isso, criando dificuldades para depois vender (de alguma forma) facilidades. 

De um jeito ou de outro, toda e qualquer sobrevida ao *modus operandi* clássico dos velhos repositórios FTP vai depender justamente de uma massa crítica que possa operar as ferramentas necessárias. Este (sub)projeto é uma possível solução para o problema, automatizando (como for necessário) os processos de espelhamento, publicação e anúncio de conteúdo para os Confederados.

A necessidade de se criar (ainda outra) ferramenta se deve às características da Confederação:

* Consumir o mínimo de recursos (incluindo largura de banda do *host*)
* Ser centralizada, mesmo que não exatamente fechada
* Ser plástica, permitindo se adaptar às mudanças de repositórios

Para fins de simplicidade, a Confederação vai se padronizar em cima das seguintes ferramentas:

* vsftpd - Daemon FTP
	+ Simples, seguro, conhecido
	+ Para quem quiser fornecer mirrors FTP 
* lftp
	+ Para mirrors locais "brute force"
	+ Necessário pois nem todos os repositórios mantêm um ALL-FILES atualizado
* "Daemon"
	+ Um serviço Daemon encarregado de:
		- manter atualizado os mirrors locais
		- registrar-se no WS da Confederação como mirror dos repos locais
		- manter atualizado seu registro no WS respectivo da Confederação
	+ Na lata, vai checar o ALL-FILES do repo remoto com o seu, se for diferente baixar, analisar e então fazer o download das diferenças.
		- Sem varrer o repo remoto, economizando recursos de rede e processamento de ambas as máquinas (local e remoto)
	+    
* "WS"
	+ Um Micro Serviço cuja função é:
		- Fornecer aos clientes uma interface para consulta do mirror "mais próximo"
		- Fornecer aos *Providers* uma interface para registro (e atualização)


## Daemon

W.I.P.


## WS

W.I.P.

