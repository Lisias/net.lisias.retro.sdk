net.lisias.retro.file.proxy.WS
===============================

Documentação do W/S Proxy Search (bleh... e que nome dou pra esse agora??)

Não botei autenticação nem controle - logo, sim, o RPi está bem suscetível à um DoS. O trabalho de redirecionamento é extremamente rápido e simples, de forma que no momento, sim, o serviço está com a bunda na janela e **vai** derrubar os servidor inteiro se sofrer um DoS.

Este serviço roda num Raspberry Pi 2011 com 256M de RAM, e come bem poucos recursos. O consumo de memória é negligível, e o de processamento é mínimo.

A idéia original, herdada do [**search.WS**](./README.search.md) é montar serviços que rodassem em computadores de recursos mínimos, daqueles que se leva para os Encontros de Retrocomputação para apoio ao evento. Continuei com o paradigma neste serviço.

Um efeito colateral **altamente** benéfico é _alta disponibilidade_!!!

Com os clients HTML sendo servidos pelo Proxy, qualquer atualização pode ser feita "seamless", uma vez que agora eu posso subir uma versão temporária no Desktop, que se registra no *Proxy* e passa a servir também os requests.

Nisso, ao derrubar o *Provider* no RPi, ele se desregistra e apenas o Desktop fica ativo enquanto eu subo a nova versão. Após o (demorado) boot no Raspberry, ambos estão novamente servindo e então posso derrubar o *Provider* no Desktop. :-)

O Proxy ainda é considerado α (alpha). Nenhuma funcionalidade está formalmente estabelecida, as interfaces podem mudar.

## Serviço REST W/S ##

Também implementado em [Python 3](https://docs.python.org/3/) . Roda bem no CPython e no [PyPy3 5.5.0](http://pypy.org/download.html), mas a simplicidade do serviço não fornecem condições para valer a pena os pontos fracos do PyPy3 (Strings e Garbage Collector).

Tô servindo no CPython mesmo.

Segue as mesmas directrizes do [**search.WS**](./README.search.md): dado o endereço base, que no momento é http_//home.lisias.net_8090/ (trocar _ por :), o request sempre segue a forma:

	/<algumacoisa>.<formatodesejado).

Não usar o `<formatodesejado>` implica numa response em `text/plain` (formato `.txt`, adequado para consultas manuais e debugging). Também reconhece .json e .jsonp (json com padding, para furar cross-domain scripting) e o format binário de serialização do Python, Pickle (protocolo 3). Sempre `GET`, e os eventuais parâmetros da Query seguem a rfc3986 (ou pelo menos, é o que o [Bottle](http://bottlepy.org/docs/dev/) me disse).

Se faz um request que existe mas com dados inconsistentes, recebe um 200 com um Response explicando o erro. Se você faz um request que não existe, recebe um 404. Se você faz um request que existe mas com dados inválidos, leva um 500 na orelha e fica por isso mesmo.

E retorna um HTTP 303 (redirect) com um corpo de mensagem simples, indicando o Sucesso da requisição. Deste ponto em diante, a bola está com o *Service Provider* que receber o request.

Toda resposta (incluindo os redirects) retorna um campo booleano chamado `error`. Se `False`, `success_msg` tem uma mensagem de confirmação (normalmente uma URL) e os demais campos são o payload da Response - dependente de Request. Se houve um erro, `error_msg` dá a razão por extenso, e `error_code` um identificador único no sistema do erro, para automatizar tratamento de erro.

### Requests implementados: ###

#### `/version.<fmt>` ####
retorna a identificação da aplicação e a versão corrente. Bom para saber se o serviço tá de pé (ping). Retorna os formatos reconhecidos pelo W/S (bom para saber o que vc pode usar na hora, já que novos formatos virão).

#### `/metadata.<fmt>?archive=<datasouce name>;entity=<entity name>` ####
retorna os *archives* reconhecidos pelos providers registrados (asimov, wos, etc), ou fornece a lista de providers que provêem um *archive* (se o parâmetro de query `archive` for fornecido). Se entity...**TODO**

Estes *archives* são prospectados dos Providers que forem se registrando no serviço.

Não há garantia de que estes providers estejam *on line* nem forneçam corretamente os serviços que informam.

Exemplos:

* Enumerando os *archives* conhecidos pelo Proxy:
	- Request: `/metadata.json`

	- Response: `{"archives": ["aminet", "asimov", "msxarchive", "wos", "whtech"], "error": false, "success_msg": "Archives:5"}`

* Enumernado os *Providers* de um determinado *archive*
	- Request: `/metadata.json?archive=asimov`

	- Response: `{"error": false, "providers": [{"error": false, "success_msg": "Provider http://home.lisias.net:8081/ last hit 5.268626", "hit": 5.268626, "url": "http://home.lisias.net:8081/"}, {"error": false, "success_msg": "Provider http://home.lisias.net:8082/ last hit 4.904613", "hit": 4.904613, "url": "http://home.lisias.net:8082/"}], "success_msg": "asimov Providers: 2", "archive": "asimov`

`hit` é um dado interno, usado para determinar quando o *Provider* foi requisitado pela última vez.


#### Requests de *Providers*

Uma vez escolhido o `<archive>`, a mecânica é sempre a mesma:

`/<archive>/<entitiy_plural>/<verb>.<fmt>?<parms>` (onde parms é rfc3986 )

O **proxy.WS** enumerará os Provider capazes de fornecer o *archive* especificado, ou retornará um erro.

Em cima dos metadados fornecidos pelos Providers, uma análise minimalista de sintaxe é feita, de forma que se forem solicitados entidades ou verbos não conhecidos pelo Provider, o proxy já retorna logo um erro e não aborrece o Provider.

Não há tratamento para os parâmetros, ou checagem se a entidade é compatível com o verbo - isso continua encargo do Provider.

O Request é satisfeito por duas maneiras, sob configuração:

##### Redirect 303

A Response *não* contem os dados solicitados, mas apenas uma Mensagem de Sucesso. O cabeçalho Location fará um redirecionamento para o *Provider* que realizará de fato a requisição.

Por uma questão de cortesia, o **proxy.WS** inclui a sua URL no cabeçalho *Referer*. Espera-se (mas não exige-se) que o cliente, ao fazer o redirecionamento, inclua este cabeçalho no request.

##### *Proxying*

O Request é executado **pelo *Proxy*** e o resultado é ecoado no Response. Este modelo é útil quando os *Providers* estão em uma subrede (Intranet) e apenas o *Proxy* está num *host* com acesso externo (*bridge*).

Por uma questão de cortesia, o **proxy.WS** inclui a URL do *Provider* no cabeçalho *Referer*.


#### Requests "Confederados" ####

O **proxy.WS** por si não faz nada. Ele precisa que Providers, por iniciativa própria, registrem-se neste serviço. Por uma questão de simetria e ética, um Provider pode se desregistrar da "Confederação" à qualquer momento - e voltar nos mesmos termos.

São as únicas chamada em todo o *stack* que não usa **GET**.


##### **POST** `/register.<fmt>` #####

Com este request, um Provider pede cadastramento na Confederação. A URL a ser usada para o *redirect* deve ser fornecida no *header* ***Referer***. O corpo da mensagem é ignorado, mas por convenção deve conter uma cópia do *Referer*.

Uma vez solicitado o cadastro, os metadados do Provider serão obtidos pelo **proxy.WS** pelas vias normais. Se algum erro ocorrer, o cadastro é negado numa mensagem de erro explicativa.

Não há controle sobre as tentativas, não há *black list*, não há *Quality of Service*.


##### **POST** `/unregister.<fmt>` #####

Com este request o Provider pode solicitar seu descadastramento na Confederação.  A URL a ser usada para o *redirect* deve ser fornecida no *header* ***Referer***. O corpo da mensagem é ignorado, mas por convenção deve conter uma cópia do *Referer*.

*No questions asked*. O descadastramento é imediato, e não ficam registros sobre o cadastro prévio. Um recadastramento pode ser feito à qualquer momento, **Como ser fosse a primeira vez** :-) .

Pede-se que, sempre que possível, os confederados se desregistrem durante o *shutdown* de seus sistemas para evitar transtornos aos clientes e, também para evitar que o **proxy.WS* precise ficar pingando o Provider para saber se ele está vivo.

Sendo o foco destes serviços serem hospedados por *hosts* limitados e/ou em condições limitantes, atitudes pró-ativas do **proxy.WS** serão limitadas ao mínimo indispensável.


### CORS

**TODO**


Endereço atual
--------------

http://search.lisias.net/retro/version
