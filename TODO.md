# *TO DO List*


## Infra e Deploy
1. Compilar e empacotar o Python 3.6:
	1. Pro Raspbian Jessie
	2. Pro OLPC
	3. Pro Devuan Jesie
2. Migrar todo mundo para Python 3.6
	1. Muita coisa melhorou depois do 3.4, em especial asyncio. A vida de todo mundo vai ficar mais fácil com o 3.6.
3. Automaticar o redeploy
	1. Ainda mais importante, automatizar a atualização do ~/configuration


## Protocol

1. Formatos de resposta adicionais:
	1. .xml (para os SOA addicts),
	2. .yaml (pros rubistas não ficarem chateados).
	3. .java (Java Serialization. Agora é possivel!).
	4. MessagePack
		- https://pypi.python.org/pypi/msgpack-python
		- https://github.com/msgpack/msgpack-python
		- O mais rápido deles, me parece!
			- https://gist.github.com/schlamar/3134391
	5. Restringir o uso de `.pickle` para intranets e fontes confiáveis autenticadas (e com canal seguro).
		- https://www.smartfile.com/blog/python-pickle-security-problems-and-solutions/
		- Ou, melhor, restringir Pickle para armazenamento, e abandonar serialização binária específica para o Python.
2. Uma interface para Unidades Confederadas *de facto* e de direito.
	1. Outros Proxies trocarão informações entre si, na expectativa de que a derrubada de um não cause a exclusão de seus confederados.
	2. Não tenho a menor idéia de como avisar aos clientes sobre o Pool de Proxies.
	3. Não tenho a menor idéia de como tratar os Serviços ***Singletons***:
		- Deve uma Unidade Federativa ecoar seus ***singletons*** para as demais?
		- Em resumo: um ***singleton*** deve ser único em toda a Confederação?
3. Adicionar suporte para `Accept-Encoding: br` (brotli) nas responses.
4. Fazer uso de streaming ao gerar as responses (ao invés de gerar tudo na memória)
	1. Vai poupar um pouco de memória nos Responses de maior tamanho
6. Bottle.py
	- Analisar e adicionar ujson (qual deles? 1.35? 1.4? ujsonc?) para agilizar a Serialização JSON no Bottle
		- Pré-análise: ferra com minhas serializações customizadas. Vai dar algum trabalho.
	- Aproveitar e usar na Serializable também!
7. net.lisias.retro.Error
	- A lista de parâmetros para formatar os textos dos Error.Code precisa ser normalizada.
		- às vezes o http method tá na frente, às vezes no meio, uma bagunça!
		- Largar o "%" e usar ".format" deve solucionar o problema sem prejudicar as mensagens e eventuais l10n.
8. Ao trabalhar com pickle, quando receber um pickle inválido retornar 400 Bad Request
	- Pra falar a verdade, todo erro de deserialização, não apenas no Pickle!
9. Nos ACL, ao esgotar os créditos do cliente retornar "402 Payment Required"

## net.lisias.retro.proxy

1. Implementar um /report decente
	1. uptime
	2. starttime
	3. hit counter
	4. outros?
2. Implementar autenticação para os serviços de terceiros se cadastrarem.
	1. controle contra abusos


## net.lisias.retro.file.miner

1. Interoprabilidade com bibliotecas Java através do [p74j](https://www.py4j.org/):
	1. Por HTTP não ficou ruim, mas por JNI vai ficar ainda mais rápido!
	2. Uma possível linha de pesquisa?
2. AppleCommander bug.
	1. [Issue](https://github.com/AppleCommander/AppleCommander/issues/18)
		0. Work In Progress
	2. Mas uma versão interina, consertada, está em produção com sucesso


## net.lisias.retro.file.search

1. Interface GOPHER.
	- O front-end ficou bacana (mas falta um Designer pra deixar aquilo decente...), mas não roda em qualquer micrinho. Micros clássicos serão melhor servidos com o Gopher.
2. *MIRRORS*. Dar suporte para mirrors!!
3. Mais Archives! Mais Archives!!
4. Implementar um mecanismo de restart do micro-serviço, para quando o processo de reconstrução do cache tiver terminado e constatado que tem que reinicializar.


## net.lisias.retro.radio

1. Formatar decentemente o response de `http://home.lisias.net:8089/webradio/credits/balance`
2. Inventar alguma coisa simples e útil para adicionar músicas a partir da página do Internet Radio (ao invés de mandar o usuário fuçar nas Search Engines!)
3. Bolar um mecanismo de black-list para músicas ofensivas ou corrompidas.
