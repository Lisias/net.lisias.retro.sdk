# *Known Issues*

## Protocol

* Não há suporte para Unidades Confederadas *de facto* e de Direito - em resumo, não há suporte para outros proxies fazerem parte do *pool*.
	* O que temos hoje são *Providers* se filiando à vários *Proxies* simultâneamente.
	* O que fazer se um Provider se desregistra de seu proxy?
		- Ecoa o desregistro para os demais?
		- Se não, deixa os demais Proxies potencialmente no escuro à respeiro deste Provider?
* Ainda não houve nenhum esforço para padronizar a nomenclatura das entidades, verbos e *archives*
	- Mas espero que não mude muito do que já tô usando, os nomes são relativamente famosos e já estabelecidos.
	- Novos verbos e novas entidades, no entanto, podem ser um problema.
* Risco de segurança é brincadeira.
	- As oportunidades para data mining, spam e distribuição de *malwares* são problemas que simplesmente sequer abordei num *brainstorming*!
	- Ainda tá meio *Far, Far West*.
	- Mecanismos de prevenção de abusos precisam ser especificados e implementados.
		1. Register & Unregister disparam um rebuild interno. A estrutura é protegida por mutex, mas um DoS no sistema é facilmente realizado com uma sucessão de registros e desregistros!
	- Pickle é um problema. Ver o [TODO](./TODO.md).

## Todos

* Há um problema com o Bottle, na hora de terminar o Daemon ele tráva com um Warning e lá fica.
	* E um problema já conhecido com o Bootle.
		- https://github.com/bottlepy/bottle/issues/814
	* Considerando as alternativas.
	* NOTA: Este problema aparenta ter "sumido" sozinho.
	* Nota2: O processo de shutdown dos WebServices agora culminam com um sys.exit(0) - dou um tiro na testa do processo e pronto.
		- Mas isso é diferente de resolver o problema descrito. :)
* Quando o WS recebe um SIGTERM (ou um CTRL-C) e ele está com a fila de tarefas cheia ("Waiting for myself" ou "Waiting for Proxy"), as threads secundárias seguram o processo e não deixam ele terminar.
	* Implementar um Tasker.Killer.
	* Colocá-lo dentro do loop de shutdown
* Migrar os Annunciators para usar o Tasker.
	* É pra isso que eu inventei estre treco!

## net.lisias.retro.proxy

* O processo de shutdown precisa de revisão.
	- Está travando por eras
		- Tem à ver com o desregistro nos proxies. Se um estiver fora do ar, ele fica lá até dar *timeout*.
		- Não tenho certeza até que ponto isso deve ser corrigido.
			- O que é mais importante: desligar rápido ou avisar seus proxies de que não está mais disponível?
		- Priorizar a persistência dos dados, e só então sair com o desregistro. Isso garante que os dados sejam salvos caso o Kernel perca a paciência pela demora e mate o processo por força bruta.
* Não há QoS

## net.lisias.retro.file.miner
* A recarga de repositórios "zombie" está frágil
	* Quando a primeira carga falha, da segunda em diante os arquivos auxiliares (se existirem) não são mais baixados.
		* Isso acontece porque, uma vez que a primeira carga é finalizada com sucesos, o que veio veio e o que não veio não vem mais - é pos isso que marquei o repositório como "zombie".
	* A solução pra isso é matar o "REFERENCE" sempre que a primeira carga não concluir com suceso.
		* Isso vai disparar uma primeira carga quando o Produtor rodar novamente.

## net.lisias.retro.file.search

* Updates de repositório só acontecem restartando o server.
	* *hot updates* ainda é research in progress, mas já estamos quase lá!
* Não tenho máquina pra todos os *Archives*! Eles estão crescendo em número e tamanho!! :-(

## net.lisias.retro.radio

* Consome um bocado de CPU.
	- Não tem pra onde fugir, é um decoder MOD seguido de um encoder MP3.
	- Não é bem um comportamento do W/S, mas do icecast2 e do MPD.
* A carga das músicas está frágil
	- Normalmente a primeira carga depois de levantar o serviço falha (!!)
	- Vez ou outra falha também no meio do caminho, o processo "mpc add" retorna com falha 1.
	- Conjecturas
		+ Estou abusando da memória neste aparelho, vez ou outra o processo shell morre.
			+ Possível, mas improvável - o arquivo tá sempre lá no filesystem, eu não achei um único caso em que o arquivo não estivesse lá, por inteiro - incluindo o arquivo extraído.
			+ Os arquivos da ModLand não vem compactados, eles não passam por este processo, mas falham também de vez em quando...
		+ Estou abusando da CPU do aparelho, vez ou outra um subprocesso tráva e a falta de um mutex faz o próximo tropeçar
			+ Mais provável. Mas o Python não é multitarefa de verdade, ele usa greenthreads e a thread que faz este serviço fica esperando o processo shell terminar antes de iniciar o outro!
		+ Bug na biblioteca Python que tá soltando o caller antes da hora
			+ Vez ou outra eu pego um bug estranho numa lib em Python. Num PC poderoso, a maioria destes bugs não passam de possibilidades teóricas, mas as coisas não são muito fáceis num ARMv7 de 800MHz...
		+ Outra possibilidade é o arquivo ainda estar 'locked' quando o `mpc add` vai buscar o cara. Improvável, mas teoricamente possível - neste caso, seria outro bug em uma lib Python.
* O fetch da música solicitada e o descompactamento podem demorar uns minutos.
	* Não tem o que fazer, exceto implementar um bloqueio temporário para o FileData para evitar reentrância do mesmo request.
* Usar o Tasker para as tarefas de background!
* Todo o serviço é α (alpha)
	- Pode mudar tudo de uma hora pra outra!


## Clientes HTML

* Os clientes WebRadio não atualizam o nome da música corrente quando troca de faixa.
	- Dar um click no texto da música para atualizar (quebra-galho, mas funciona)
	- Não é um problema do net.lisias.retro.file.mpd , é algo que tem que ser implementado na página HTML para receber os eventos do icecast2.
	- O fetch da música solicitada e o descompactamento podem demorar uns minutos.
		- O usuário do outro lado do cable-modem fica sem saber o que tá acontecendo, e acaba clicando de novo.
		- Bolar uma forma de avisar o usuário da demora.
* A página do Scene Org está horrivelmente lenta na hora de preencher `keywords`.
	- A atual estratégia de *autocomplete* está exaurida. Há *keywords* demais para que isso funcione bem do jeito que está.
	- Uma solução para isso é WiP.
		- Mas dá pra adiantar que vai envolver recuperações parciais da lista de *keywords*, algo já implementado no *back-end*.


## Clientes Console Python

* examples/webradio2.py
	* Este treco não funciona para streams codificados - só para PCM puro.
	* Não mudarei isso tão cedo...
* examples/webradio3.py
	* Python3 only
	* Latências na rede geram silêncios no meio do streaming.
		* Aumentar os buffers mitiga o problema, mas aumenta a latência entre os eventos no Web Radio e você percebê-los no cliente
			* Por exemplo, trocar a música corrente.


## Clientes Desktop

* Java
	* WiP ! :-)


## Clientes Android

* WiP! :-)


## Clientes iOS

* RiP =P
