'''
Created on 13 de jan de 2017

@author: lisias
'''

import hmac, hashlib, datetime, binascii

def sign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).digest()

def hsign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).hexdigest()

def hdigest(msg):
    return  hashlib.sha256(msg).hexdigest()

if __name__ == '__main__':
    x = sign(b'123456', '123456')
    print(str(binascii.hexlify(x), encoding='UTF-8'))
    x = hsign(b'123456', '123456')
    print(x)
    x = hdigest(b'123456')
    print(x)
